<?php

if (!function_exists('getCountKategori')) {
    function getCountKategori() {

        return \DB::table('kategori')->count();
    }
}

if (!function_exists('getCountPortofolio')) {
    function getCountPortofolio($idKategori = null) {

        if($idKategori) {
            return \DB::table('portofolios')->where('kategori', '=', $idKategori)->count();
        }

        return \DB::table('portofolios')->count();
    }
}

if (!function_exists('getCountClient')) {
    function getCountClient() {

        return \DB::table('clients')->count();
    }
}

if (!function_exists('getCountInbox')) {
    function getCountInbox() {

        return \DB::table('contacts')->where('read', 0)->count();
    }
}

if (!function_exists('getThumbnail')) {
    function getThumbnail($idPorto) {

        $porto =  \DB::table('portofolios')->where('id', '=', $idPorto)->first();

        if (!$porto) {
            return $idPorto;
        }

        return $porto->path . '/' . $porto->file_name;
    }
}

if (!function_exists('getClientName')) {
    function getClientName($id) {

        if ($id == 0) {
            return '-';
        }

        $result = \DB::table('clients')->where('id', '=', $id)->first();

        return $result->clientName;
    }
}

if (!function_exists('getKategoriName')) {
    function getKategoriName($id) {

        $result = \DB::table('kategori')->where('id', '=', $id)->first();

        return $result->name;
    }
}

if (!function_exists('getKategoriAlias')) {
    function getKategoriAlias($id) {

        $result = \DB::table('kategori')->where('id', '=', $id)->first();

        return $result->alias;
    }
}

if (!function_exists('getLastUpdate')) {
    function getLastUpdate($table) {

        $result = \DB::table($table)->orderBy('updated_at', 'desc')->first();

        if (!$result) {
            return null;
        }

        return $result->updated_at;
    }
}

if (!function_exists('getPattern')) {
    function getPattern($id) {

        $result = \DB::table('svg_pattern')->where('id', $id)->first();

        return $result->pattern;
    }
}

if (!function_exists('getLogo')) {
    function getLogo() {

        $result = \DB::table('informations')->where('id', 1)->first();

        $logo = unserialize($result->logos);

        if (!array_key_exists('primary', $logo)) {
            return null;
        }

        $return = $logo['primary']['path'] . '/' . $logo['primary']['filename'];

        return $return;
    }
}

if (!function_exists('getLogoSec')) {
    function getLogoSec() {

        $result = \DB::table('informations')->where('id', 1)->first();

        $logo = unserialize($result->logos);

        if (!array_key_exists('secondary', $logo)) {
            return null;
        }

        $return = $logo['secondary']['path'] . '/' . $logo['secondary']['filename'];

        return $return;
    }
}

if (!function_exists('getExperience')) {
    function getExperience($information) {
        $exp = unserialize($information->info)['start_working'];

        return $exp;
    }
}

if (!function_exists('getSocialsLink')) {
    function getSocialsLink($information) {
        $socials = unserialize($information->socials);

        return $socials;
    }
}

if (!function_exists('getUnreadInbox')) {
    function getUnreadInbox() {

        return \DB::table('contacts')->where('read', 0)->count();
    }
}

if (!function_exists('getLastInbox')) {
    function getLastInbox() {

        $result = \DB::table('contacts')->where('read', 0)->orderBy('created_at', 'desc')->first();

        if (!$result) {
            return null;
        }

        return $result->created_at;
    }
}
