<?php

namespace App\Http\Controllers;

date_default_timezone_set('Asia/Jakarta');

use App\Mail\PikapikalabMail;
use App\Models\Contacts;
use App\Models\DeleteRecord;
use App\Models\Informations;
use App\Models\Kategori;
use App\Models\Portofolio;
use App\Models\Client;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Response;


class PikapikalabController extends Controller
{
    /*=====================================[LANDINGPAGE SECTION]=============================================*/
    /**
     * landing page
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataMenu = Kategori::all();
        $dataClient = Client::all();
        $dataInfo   = Informations::find(1);

        $data = [
            'categories' => $dataMenu,
            'clients'    => $dataClient,
            'info'       => $dataInfo
        ];

        return view('welcome', $data);
    }

    /**
     * @param $alias
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function viewPortofolio($alias)
    {
        $category = Kategori::where('alias', $alias)->first();
        $dataInfo   = Informations::find(1);
        $dataPorto  = Portofolio::where('kategori', $category->id)->get();

        $related_category = Kategori::where('id', '!=', $category->id)->where('display', 1)->orderBy(DB::raw('RAND()'))->limit(3)->get();

        $data = [
            'category' => $category,
            'info'       => $dataInfo,
            'porto'      => $dataPorto,
            'alias'      => $alias,
            'otherCategory' => $related_category,
        ];

        return view('landingpage/portofolio/portofolioCategory', $data);
    }

    /**
     * @param $alias
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function viewPortofolioItem($alias, $filename)
    {
        $dataPorto  = Portofolio::where('file_name', $filename)->first();
        $dataInfo   = Informations::find(1);

        $related_item = Portofolio::where('id', '!=', $dataPorto->id)
            ->where('kategori', $dataPorto->kategori)
            ->where('display', 1)
            ->orderBy(DB::raw('RAND()'))
            ->limit(3)->get();

        $category = Kategori::where('id', $dataPorto->kategori)->first();

        $data = [
            'porto'      => $dataPorto,
            'info'       => $dataInfo,
            'alias'      => $category->alias,
            'otherItem'  => $related_item,
        ];

        return view('landingpage/portofolio/portofolioItem', $data);
    }

    /*=====================================[ADMIN VIEW SECTION]=============================================*/

    /**
     * login page
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse|Redirector
     */
    public function login()
    {
        $loginStatus = Session::get('login');

        if (!$loginStatus) {
            return view('/admin/login');
        }

        return redirect('/admin/dashboard');
    }

    /**
     * admin page
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function dashboard()
    {
        $loginStatus = Session::get('login');

        if (!$loginStatus) {
            return view('/admin/login');
        }

        return view('admin/dashboard');
    }

    /**
     * kategori page
     * @param null $alias
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function kategori($alias = null)
    {
        $loginStatus = Session::get('login');

        if (!$loginStatus) {
            return view('/admin/login');
        }

        if ($alias) {
            $kategori = Kategori::where('alias', $alias)->first();
            $porto = Portofolio::where('kategori', $kategori->id)->get();

            $data = [
                'kategori' => $kategori,
                'porto' => $porto
            ];

            return view('admin/viewPortofolio', $data);
        }

        $dataMenu = Kategori::all();

        $data = [
            'kategoriList' => $dataMenu
        ];

        return view('admin/kategori', $data);
    }

    /**
     * portofolio page
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function portofolio()
    {
        $loginStatus = Session::get('login');

        if (!$loginStatus) {
            return view('/admin/login');
        }

        $dataMenu = Kategori::all();
        $dataClient = Client::all();
        $dataPorto = Portofolio::orderBy('created_at', 'desc')->paginate(7);

        $data = [
            'kategoriList' => $dataMenu,
            'clientList' => $dataClient,
            'portofolioList' => $dataPorto,
        ];

        return view('admin/portofolio', $data);
    }

    /**
     * client page
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function client()
    {
        $loginStatus = Session::get('login');

        if (!$loginStatus) {
            return view('/admin/login');
        }

        $dataClient = Client::orderBy('created_at', 'desc')->paginate(7);
        $data = [
            'clients' => $dataClient,
        ];

        return view('admin/client', $data);
    }

    /**
     * manage info page
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function manageInfo()
    {
        $loginStatus = Session::get('login');

        if (!$loginStatus) {
            return view('/admin/login');
        }

        $dataInfo = Informations::find(1)->first();
        $data = [
            'info' => $dataInfo,
        ];

        return view('admin/manageInfo', $data);
    }

    /**
     * manage inbox page
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function manageInbox()
    {
        $loginStatus = Session::get('login');

        if (!$loginStatus) {
            return view('/admin/login');
        }

        $dataInbox = Contacts::orderBy('created_at', 'desc')->paginate(10);
        $data = [
            'mails' => $dataInbox,
        ];

        return view('admin/manageInbox', $data);
    }

    /*=====================================[STORE SECTION]=============================================*/

    /**
     * @return Application|RedirectResponse|Redirector
     */
    public function logout()
    {
        Session::flush();
        return redirect('/admin')->with('success', 'Silahkan Login');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendEmail(Request $request) {
        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');

        $checkEmail = Contacts::where('name', $name)
            ->where('email', $email)
            ->where('read', 0)
            ->count();

        if ($checkEmail > 3) {
            return Response::json([
                'status' => false,
                'msg' => 'Your sending email too much, try it later.'
            ], 200);
        }

        $newEmail = new Contacts();
        $newEmail->created_at = Carbon::now();
        $newEmail->name    = $name;
        $newEmail->email   = $email;
        $newEmail->subject = $subject;
        $newEmail->message = $message;
        $newEmail->read    = 0;
        $newEmail->reply   = 0;

        $newEmail->save();

        Mail::to('priska@pikapikalab.id')
            ->send(new PikapikalabMail($email, $name, $subject, $message));

        return Response::json([
            'status' => true,
            'msg' => 'Succesfully send email'
        ], 200);
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function loginPost(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $data = DB::table('users')->where('username', $username)->first();

        if (!$data) {
            return redirect('/admin')->with('alert', 'Password atau Username, Salah!');
        }

        if (Hash::check($password, $data->password)) {

            Session::put('id', $data->id);
            Session::put('username', $data->name);
            Session::put('login', TRUE);

            return redirect('/admin/dashboard')->with('success', 'Login success, welcome ' . $data->name);

        } else {
            return redirect('/admin')->with('error', 'Password atau Username, Salah !');
        }
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function checkOldPassword(Request $request)
    {
        $userID = $request->userID;
        $password = $request->oldPassword;

        $data = User::find($userID);

        if (!$data) {
            return Response::json([
                'status' => false,
                'msg' => 'User not found'
            ], 200);
        }

        if (!Hash::check($password, $data->password)) {

            return Response::json([
                'status' => false,
                'msg' => 'Wrong password'
            ], 200);

        }

        return Response::json([
            'status' => true,
        ], 200);
    }

    /**
     * @param Request $request
     */
    public function changePassword(Request $request) {

        $userID = $request->userID;
        $newPassword = Hash::make($request->newPassword);

        $data = User::find($userID);

        if (!$data) {
            return Response::json([
                'msg' => 'User not found.',
            ], 4401);
        }

        $data->password = $newPassword;
        $data->updated_at = Carbon::now();

        $data->save();

        return Response::json([
            'msg' => 'Successfully change password.',
        ], 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addKategori(Request $request)
    {
        $name = $request->input('name');
        $thumbnail = $request->input('thumbnail');
        $alias = $request->input('alias');
        $color = $request->input('color');
        $desc = $request->input('desc');
        $path = 'portofolio/' . $alias;

        $kategori = new Kategori();

        $kategori->created_at = Carbon::now();
        $kategori->created_by = Session::get('id');
        $kategori->name = ucfirst($name);
        $kategori->alias = strtolower($alias);
        $kategori->color = strtolower($color);
        $kategori->deskripsi = $desc ? $desc : '';
        $kategori->thumbnail = $thumbnail;
        $kategori->display = 1;
        $kategori->folder_path = $path;

        $kategori->save();

        return Response::json([
            'msg' => 'Add Kategory Success'
        ], 200);

    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function editKategori($id, Request $request)
    {
        $name = $request->input('nameEdit');
        $alias = $request->input('aliasEdit');
        $thumbnail = $request->input('thumbnailEdit');
        $color = $request->input('colorEdit');
        $desc = $request->input('descEdit');

        $kategori = Kategori::find($id);

        $kategori->updated_at = Carbon::now();
        $kategori->updated_by = Session::get('id');
        $kategori->name = ucfirst($name);
        $kategori->alias = strtolower($alias);
        $kategori->thumbnail = $thumbnail;
        $kategori->color = strtolower($color);
        $kategori->deskripsi = $desc ? $desc : '';
        $kategori->display = 1;

        $kategori->save();

        return Response::json([
            'msg' => 'Edit Kategory Success'
        ], 200);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatusDisplay(Request $request)
    {
        $id = $request->input('id');
        $status = $request->input('display');

        $kategori = Kategori::find($id);
        $kategori->updated_at = Carbon::now();
        $kategori->updated_by = Session::get('id');
        $kategori->display = $status;
        $kategori->save();

        return Response::json([], 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function storeUploadClient(Request $request)
    {
        if (!$request->hasFile('fileUpload')) {
            return Response::json([
                'errMsg' => 'No File is attached !'
            ], 400);
        }

        $path = 'public/client';

        if (!Storage::disk('public')->has($path)) {
            Storage::disk('public')->makeDirectory($path);
        }

        $clientName = $request->input('clientName');
        $file = $request->file('fileUpload');
        $extension = $file->getClientOriginalExtension();
        $newFileName = str_replace(' ', '-', strtolower($clientName)) . '_' . date('ymd') . '.' . $extension;

        $isMoved = $file->storePubliclyAs($path, $newFileName);

        if (!$isMoved) {
            return Response::json([
                'errMsg' => 'Failed uploading Image !'
            ], 400);
        }

        $client = new Client();

        $client->created_at = Carbon::now();
        $client->created_by = Session::get('id');
        $client->path = str_replace('public', 'storage', $path);
        $client->file_name = $newFileName;
        $client->clientName = $clientName;

        $client->save();

        return Response::json([], 200);

    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function storeEditClient($id, Request $request)
    {
        $clientName = $request->input('clientName');
        $path = 'public/client';

        $client = Client::find($id);

        if ($request->hasFile('fileUpload')) {
            $destinationPath = $client->path . '/' . $client->file_name;
            Storage::disk('public')->delete($destinationPath);

            $file = $request->file('fileUpload');
            $extension = $file->getClientOriginalExtension();
            $newFileName = str_replace(' ', '-', strtolower($clientName)) . '_' . date('ymd') . '.' . $extension;

            $isMoved = $file->storePubliclyAs($path, $newFileName);
            $client->file_name = $newFileName;
        }

        $client->updated_at = Carbon::now();
        $client->updated_by = Session::get('id');
        $client->clientName = $clientName;

        $client->save();

        return Response::json([
            'msg' => 'Edit Client Success'
        ], 200);

    }

    /**
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function deleteClient($id)
    {
        $client = Client::find($id);

        $deleted = new DeleteRecord();

        $deleted->created_at = Carbon::now();
        $deleted->updated_by = 0;
        $deleted->user_id    = Session::get('id');
        $deleted->table_name = 'clients';
        $deleted->status     = 'DELETED';
        $deleted->record     = serialize($client->getAttributes());

        $deleted->save();

        $destinationPath = $client->path . '/' . $client->file_name;
        Storage::disk('public')->delete($destinationPath);

        $client->delete();

        $getPage = $_GET['page'];
        return redirect('admin/client' . ($getPage ? '?page=' . $getPage : ''))->with('success', 'Success Delete client ID ' . $id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function storeUploadPortofolio(Request $request)
    {
        $idKategori = $request->input('kategori');
        $idClient = $request->input('client');
        $desc     = $request->input('deskripsi');
        $kategori = Kategori::find($idKategori);

        if (!$request->hasFile('fileUpload')) {
            return Response::json([
                'errMsg' => 'No File is attached !'
            ], 400);
        }

        if (!$kategori->folder_path) {
            $path = 'portofolio/' . $kategori->alias;

            if (!Storage::disk('public')->has($path)) {
                Storage::disk('public')->makeDirectory($path);
                $kategori->updated_at = Carbon::now();
                $kategori->folder_path = $path;
            }
        }

        $file = $request->file('fileUpload');
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $extension = $file->getClientOriginalExtension();
        $newFileName = $filename . '_' . date('ymd') . '.' . $extension;
        $path = 'public/' . $kategori->folder_path;

        $isMoved = $file->storePubliclyAs($path, $newFileName);

        if (!$isMoved) {
            return Response::json([
                'errMsg' => 'Failed uploading Image !'
            ], 400);
        }

        $portofolio = new Portofolio();

        $portofolio->created_at = Carbon::now();
        $portofolio->created_by = Session::get('id');
        $portofolio->kategori   = $idKategori;
        $portofolio->client_id  = $idClient;
        $portofolio->path       = str_replace('public', 'storage', $path);;
        $portofolio->file_name  = $newFileName;
        $portofolio->deskripsi  = $desc;
        $portofolio->display    = 1;

        $portofolio->save();

        $kategori->thumbnail = $kategori->thumbnail ? $kategori->thumbnail : $portofolio->id;
        $kategori->save();

        return Response::json([], 200);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatusDisplayPortofolio(Request $request)
    {
        $id = $request->input('id');
        $status = $request->input('display');

        $porto = Portofolio::find($id);
        $porto->updated_at = Carbon::now();
        $porto->updated_by = Session::get('id');
        $porto->display = $status;
        $porto->save();

        return Response::json([], 200);
    }

    /**
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function deletePortofolio($id)
    {
        $porto = Portofolio::find($id);

        $deleted = new DeleteRecord();

        $deleted->created_at = Carbon::now();
        $deleted->updated_by = 0;
        $deleted->user_id    = Session::get('id');
        $deleted->table_name = 'portofolios';
        $deleted->status     = 'DELETED';
        $deleted->record     = serialize($porto->getAttributes());

        $deleted->save();

        $destinationPath = $porto->path . '/' . $porto->file_name;
        Storage::disk('public')->delete($destinationPath);

        $porto->delete();

        $getPage = $_GET['page'];
        return redirect('admin/portofolio' . ($getPage ? '?page=' . $getPage : ''))->with('success', 'Success Delete portofolio ID ' . $id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function storeEditPortofolio($id, Request $request)
    {
        $kategori = $request->input('kategori');
        $client   = $request->input('client');
        $desc     = $request->input('deskripsi');

        $porto = Portofolio::find($id);

        $porto->updated_at = Carbon::now();
        $porto->updated_by = Session::get('id');
        $porto->kategori  = $kategori;
        $porto->client_id = $client;
        $porto->deskripsi = $desc;

        $porto->save();

        return Response::json([
            'msg' => 'Edit Portofolio Success'
        ], 200);
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function storeEditInfo($id, Request $request)
    {
        $path = 'public/logo';

        if (!Storage::disk('public')->has($path)) {
            Storage::disk('public')->makeDirectory($path);
        }

        $logo   = [];
        $social = [];
        $other  = [];

        $info   = Informations::find($id);

        if (!$info) {
            $info = new Informations();
        }

        if ($request->hasFile('logoPrimary')) {
            if ($info && $info->logos) {
                $logoPrimary = unserialize($info->logos);

                if (array_key_exists('primary', $logoPrimary)) {
                    $destinationPathPrimary = $logoPrimary['primary']['path'] . '/' . $logoPrimary['primary']['filename'];
                    Storage::disk('public')->delete($destinationPathPrimary);
                }
            }

            $file = $request->file('logoPrimary');
            $extension = $file->getClientOriginalExtension();
            $newFileName = 'primary_logo.' . $extension;

            $isMoved = $file->storePubliclyAs($path, $newFileName);
            $logo['primary']['path']     =  str_replace('public', 'storage', $path);
            $logo['primary']['filename'] = $newFileName;
        } else {
            $logo['primary'] = unserialize($info->logos)['primary'];
        }


        if ($request->hasFile('logoSec')) {
            if ($info && $info->logos) {
                $logoSec = unserialize($info->logos);

                if (array_key_exists('secondary', $logoSec)) {
                    $destinationPathSec = $logoSec['secondary']['path'] . '/' . $logoSec['secondary']['filename'];
                    Storage::disk('public')->delete($destinationPathSec);
                }
            }

            $file = $request->file('logoSec');
            $extension = $file->getClientOriginalExtension();
            $newFileName = 'secondary_logo.' . $extension;

            $isMoved = $file->storePubliclyAs($path, $newFileName);
            $logo['secondary']['path']     = str_replace('public', 'storage', $path);
            $logo['secondary']['filename'] = $newFileName;
        } else {
            $logo['secondary'] = unserialize($info->logos)['secondary'];
        }

        //socials
        $social['linkedin'] = $request->input('linked_in');
        $social['dribble'] = $request->input('dribble');
        $social['instagram'] = $request->input('instagram');

        //other info
        $other['start_working'] = $request->input('start_working');

        $info->updated_at = Carbon::now();
        $info->updated_by = Session::get('id');

        $info->logos   = serialize($logo);
        $info->socials = serialize($social);
        $info->info    = serialize($other);

        $info->save();

        return Response::json([
            'msg' => 'Edit Information Success'
        ], 200);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function readMessage($id)
    {
        if ($id == 'all') {
            DB::table('contacts')
                ->where('read', '=', 0)
                ->update([
                    'read' => 1,
                    'updated_at' => Carbon::now()
                ]);

            return Response::json([], 200);
        }

        $inbox = Contacts::find($id);
        $inbox->updated_at = Carbon::now();
        $inbox->read = 1;
        $inbox->save();

        return Response::json([], 200);
    }
}
