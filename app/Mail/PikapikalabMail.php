<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PikapikalabMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fromMail, $name, $subjectMail, $message)
    {
        $this->fromMail = $fromMail;
        $this->nameSender = $name;
        $this->subjectMail = $subjectMail;
        $this->contentMail = $message;
    }

    /**
     * Build the message.
     *
     * @return PikapikalabMail
     */
    public function build()
    {
        return $this
            ->subject($this->subjectMail)
            ->from($this->fromMail)
            ->view('email')
            ->with([
                'name' => $this->nameSender,
                'content' => $this->contentMail,
            ]);
    }
}
