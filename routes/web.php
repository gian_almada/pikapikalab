<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PikapikalabController@index');
Route::get('/portofolio/{alias}', 'PikapikalabController@viewPortofolio');
Route::get('/portofolio/{alias}/{filename}', 'PikapikalabController@viewPortofolioItem');
Route::get('/logout', 'PikapikalabController@logout');

/* POST */
Route::post('/loginPost', 'PikapikalabController@loginPost');
Route::post('/checkOldPassword', 'PikapikalabController@checkOldPassword');
Route::post('/changePassword', 'PikapikalabController@changePassword');
Route::post('/contact/send-email', 'PikapikalabController@sendEmail');

Route::group(['prefix' => 'admin'], function()
{
    Route::get('/', 'PikapikalabController@login');
    Route::get('/dashboard', 'PikapikalabController@dashboard');

    Route::group(['prefix' => 'category'], function()
    {
        Route::get('/{alias?}', 'PikapikalabController@kategori');
        Route::post('/add', 'PikapikalabController@addKategori');
        Route::post('/edit/{id}', 'PikapikalabController@editKategori');
        Route::post('/display', 'PikapikalabController@changeStatusDisplay');
    });

    Route::group(['prefix' => 'portofolio'], function()
    {
        Route::get('/', 'PikapikalabController@portofolio');
        Route::post('/add', 'PikapikalabController@storeUploadPortofolio');
        Route::post('/display', 'PikapikalabController@changeStatusDisplayPortofolio');
        Route::post('/edit/{id}', 'PikapikalabController@storeEditPortofolio');
        Route::get('/delete/{id}', 'PikapikalabController@deletePortofolio');
    });

    Route::group(['prefix' => 'client'], function()
    {
        Route::get('/', 'PikapikalabController@client');
        Route::post('/add', 'PikapikalabController@storeUploadClient');
        Route::post('/edit/{id}', 'PikapikalabController@storeEditClient');
        Route::get('/delete/{id}', 'PikapikalabController@deleteClient');
    });

    Route::group(['prefix' => 'info'], function()
    {
        Route::get('/', 'PikapikalabController@manageInfo');
        Route::post('/edit/{id}', 'PikapikalabController@storeEditInfo');
    });

    Route::group(['prefix' => 'inbox'], function()
    {
        Route::get('/', 'PikapikalabController@manageInbox');
        Route::get('/read/{id}', 'PikapikalabController@readMessage');
    });
});

Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});

Route::get('/publish', function () {
    Artisan::call('vendor:publish');
});

Route::get('/routecache', function () {
    Artisan::call('route:clear');
    Artisan::call('route:cache');
});

Route::get('/configclear', function () {
    Artisan::call('config:cache');
    Artisan::call('config:clear');
});
