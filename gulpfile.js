const {src, dest, watch, series} = require('gulp'),
    compileSass = require('gulp-sass')(require('node-sass')),
    minifyCss = require('gulp-minify-css'),
    sourceMaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat');

const bundleLandingPageSass = function () {
    return src('./public/assets/landingpage/scss/*.scss')
        .pipe(sourceMaps.init())
        .pipe(compileSass().on('error', compileSass.logError))
        .pipe(minifyCss())
        .pipe(sourceMaps.write())
        .pipe(concat('landingpage-bundle.min.css'))
        .pipe(dest('./public/assets/landingpage/css/'));
}

const watchLandingPageCss = function () {
    watch(
        './public/assets/landingpage/scss/*scss',
        series(bundleLandingPageSass, bundleCssSourceLandingpage)
    );
}

const bundleCssSourceLandingpage = () => {
    return src([
        './public/assets/landingpage/vendor/bootstrap/css/bootstrap.min.css',
        './public/assets/landingpage/vendor/venobox/venobox.css',
        './public/assets/landingpage/vendor/owl.carousel/assets/owl.carousel.min.css',
        './public/assets/landingpage/vendor/aos/aos.css',
        './public/assets/landingpage/css/landingpage-bundle.min.css',
    ])
        .pipe(concat('landingpage-bundle.css'))
        .pipe(dest('./public/css/'));
}

exports.bundleLandingPageSass  = bundleLandingPageSass;

//bundle source
exports.bundleCssSourceLandingpage = bundleCssSourceLandingpage;

//watch
exports.watch_lp_css  = watchLandingPageCss;
