<!DOCTYPE html>
<html lang="en">
<head>
    @include('landingpage.layout.head')
</head>
<body>

@include('landingpage.section.header')

@include('landingpage.section.hero')

<main id="main">
    @include('landingpage.section.portofolioSection')

    @include('landingpage.section.countSection')

    @include('landingpage.section.clientSection')
</main><!-- End #main -->

<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Contact me</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formContact" data-parsley-validate="">
                <div class="modal-body mx-3">
                    <div class="md-form mb-2">
                        <label data-error="wrong" data-success="right" for="form34"><i class='bx bx-user' ></i> Your name</label>
                        <input type="text" id="clientName" name="name" class="form-control validate" required
                               data-parsley-name=""
                               data-parsley-trigger="focusout">
                    </div>

                    <div class="md-form mb-2">
                        <label data-error="wrong" data-success="right" for="form29"><i class='bx bx-envelope' ></i> Your email</label>
                        <input type="email" id="clientEmail" name="email" class="form-control validate" required
                               data-parsley-email=""
                               data-parsley-type="email"
                               data-parsley-trigger="focusout">
                    </div>

                    <div class="md-form mb-2">
                        <label data-error="wrong" data-success="right" for="form32"><i class='bx bx-tag' ></i> Subject</label>
                        <input type="text" id="clientSubject" name="subject" class="form-control validate" required
                               data-parsley-subject=""
                               data-parsley-trigger="focusout">
                    </div>

                    <div class="md-form">
                        <label data-error="wrong" data-success="right" for="form8"><i class='bx bx-message-detail text-gray prefix'></i> Your message</label>
                        <textarea type="text" id="clientMessage" name="message" class="md-textarea form-control" rows="4" required
                                  data-parsley-message=""
                                  data-parsley-trigger="focusout"></textarea>
                    </div>

                </div>
                <div class="modal-footer d-flex justify-content-end">
                    <button type="button" id="submitEmail" class="btn btn-custom btn-unique">Send <i class='bx bxs-paper-plane'></i></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    let Contact = {
        CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),
        formContact: $('#formContact'),
        btnSubmit: $('#submitEmail'),
        modalContact: $('#modalContactForm'),

        init: function () {
            Contact.initBtnSubmit();
        },

        initBtnSubmit: function () {
            Contact.btnSubmit.click(function (e) {
                e.preventDefault();

                if (!Contact.formContact.parsley().validate()) {
                    return;
                }

                $(this).prop('disabled', true).text('Saving ..');
                Contact.doSubmit();
            });
        },

        doSubmit: function () {
            let url = '/contact/send-email',
                data = new FormData(Contact.formContact[0]),
                type = 'POST';

            Contact.submitForm(url, data, type);
        },

        submitForm: function (url, data, type) {

            return $.ajax({
                headers: {'X-CSRF-TOKEN': Contact.CSRF_TOKEN},
                url : url,
                type : type,
                method: type,
                data : data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data){
                    Contact.modalContact.modal('hide');

                    if (data.status == true) {
                        console.log('test')
                        swal.fire({
                            title: 'Success',
                            text: data.msg,
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Close'
                        }).then((result) => {
                            if(result){
                                Contact.formContact.parsley().reset();
                                Contact.formContact.trigger('reset');
                                Contact.btnSubmit.prop('disabled', false).html('Send <i class=\'bx bxs-paper-plane\'></i>');
                            }
                        });
                    } else {
                        swal.fire({
                            title: 'Failed!',
                            text: data.msg,
                            icon: 'error',
                            confirmButtonText: 'Close'
                        });
                    }
                },
                error: function (xhr, status, error){

                    swal.fire({
                        title: 'Failed!',
                        text: xhr.responseText,
                        icon: 'error',
                        confirmButtonText: 'Close'
                    });
                }
            });
        }

    }

    window.addEventListener('load', Contact.init);
</script>

<!-- ======= Footer ======= -->
<footer id="footer">
    @include('landingpage.section.footer')
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
<div id="preloader"></div>

    @include('landingpage.layout.scriptLoad')

</body>
</html>
