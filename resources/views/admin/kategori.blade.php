@extends('admin.template.templateAdmin')

@section('title', 'Kategori Page')

@section('header', 'Kategori')

@section('content')
    <div class="row">
        <div class="col-12 mb-4">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">Add New</button>
        </div>
    </div>
    <div class="row">
        <?php foreach ($kategoriList as $kategori): ?>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 d-flex">
                <div class="card card-stats">
                    <div class="card-header card-header-icon mb-auto">
                        <div class="card-icon"  <?php if ($kategori->color):?> style="background-color: {{'#'.$kategori->color}} !important; background-image: unset !important;"<?php endif; ?>>
                            <i class="bx {{ $kategori->thumbnail ? $kategori->thumbnail : 'bx-loader-circle' }} bx-tada"></i>
                        </div>
                    <p class="card-category"><a href="javascript:" data-toggle="modal" data-target="#modalEdit"
                                                class="editKategori"
                                                data-kategori='<?= htmlspecialchars(json_encode($kategori), ENT_QUOTES, "UTF-8") ?>'><?= $kategori->name ?></a>
                    </p>
                    <h3 class="card-title"><?= getCountPortofolio($kategori->id) ?></h3>
                </div>
                <div class="card-footer">
                    <div class="stats align_stats">
                        <button type="button" class="btn <?php if (getCountPortofolio($kategori->id) > 0):?>btn-primary<?php else:?>btn-secondary<?php endif; ?> btn-sm"
                                <?php if (getCountPortofolio($kategori->id) < 1):?> disabled <?php endif; ?>
                                onclick="location.href='{{ url("/admin/category/" . $kategori->alias) }}'"
                                title="View portofolio on this category">View
                        </button>
                        <div class="pl-1">
                            <i class="material-icons">update</i> {{ Carbon\Carbon::parse($kategori->created_at)->diffForHumans() }}
                        </div>
                    </div>
                    <div class="stats align_stats">
                        <div class="togglebutton">
                            <label>
                                <input type="checkbox" <?php if($kategori->display == 1): ?>checked=""
                                       <?php endif; ?> data-id="<?= $kategori->id ?>">
                                <span class="toggle" title="Show on landing page"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="material-icons">clear</i>
                    </button>
                </div>
                <form id="formAddKategori" data-parsley-validate="" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Name
                            </label>
                            <input type="text" name="name" required class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Alias
                            </label>
                            <input type="text" name="alias" required class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Icon
                            </label>
                            <input type="text" name="thumbnail" required class="form-control">
                            <a class="link-icon-reference" href="https://boxicons.com/" target="_blank" rel="nofollow">Open Icon Reference Website</a>
                        </div>
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Color
                            </label>
                            <div class="input-group">
                                <input type="text" name="color" required class="form-control" value="3e9bdd" id="addColorField">
                                <button class="btn btn-primary btn-color-picker" id="addColorPicker" style="background-color: #3e9bdd">Choose color</button>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label>
                                Description
                            </label>
                            <div class="form-group">
                                <label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty,
                                    I'm in that two seat Lambo.
                                </label>
                                <textarea class="form-control" name="desc" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-right" id="submitAddForm">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="material-icons">clear</i>
                    </button>
                </div>
                <form id="formEditKategori" data-parsley-validate="" enctype="multipart/form-data">
                    <input name="idEdit" id="editId" value="" type="hidden">
                    <div class="modal-body">
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Name
                            </label>
                            <input type="text" name="nameEdit" id="editName" required class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Alias
                            </label>
                            <input type="text" name="aliasEdit" id="editAlias" required class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Icon
                            </label>
                            <input type="text" name="thumbnailEdit" id="editThumbnail" required class="form-control">
                            <a class="link-icon-reference" href="https://boxicons.com/" target="_blank" rel="nofollow">Open Icon Reference Website</a>
                        </div>
                        <div class="form-group mb-3">
                            <label class="bmd-label-floating">
                                Color
                            </label>
                            <div class="input-group">
                                <input type="text" name="colorEdit" required class="form-control" id="editColorField">
                                <button class="btn btn-color-picker" id="editColorPicker">Choose color</button>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label>
                                Description
                            </label>
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                </label>
                                <textarea class="form-control" id="editDesc" name="descEdit"
                                          rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-right" id="submitEditForm">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Gallery Modal -->
    <div class="modal fade" id="modalGallery" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Gallery</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="material-icons">clear</i>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Gallery -->
                    <div class="row">
                        <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
                            <a data-fancybox="gallery" href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73).jpg">
                                <img
                                    src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73).jpg"
                                    class="w-100 shadow-1-strong rounded mb-4"
                                    alt=""
                                />
                            </a>

                            <img
                                src="https://mdbootstrap.com/img/Photos/Vertical/mountain1.jpg"
                                class="w-100 shadow-1-strong rounded mb-4"
                                alt=""
                            />
                        </div>

                        <div class="col-lg-4 mb-4 mb-lg-0">
                            <img
                                src="https://mdbootstrap.com/img/Photos/Vertical/mountain2.jpg"
                                class="w-100 shadow-1-strong rounded mb-4"
                                alt=""
                            />

                            <img
                                src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73).jpg"
                                class="w-100 shadow-1-strong rounded mb-4"
                                alt=""
                            />
                        </div>

                        <div class="col-lg-4 mb-4 mb-lg-0">
                            <img
                                src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                                class="w-100 shadow-1-strong rounded mb-4"
                                alt=""
                            />

                            <img
                                src="https://mdbootstrap.com/img/Photos/Vertical/mountain3.jpg"
                                class="w-100 shadow-1-strong rounded mb-4"
                                alt=""
                            />
                        </div>
                    </div>
                    <!-- Gallery -->
                </div>
            </div>
        </div>
    </div>

@endsection
