@extends('admin.template.templateAdmin')

@section('title', 'Info Page')

@section('header', 'Manage Info')

@section('content')
    <div class="row">
        <div class="col-md-7" id="infoSection">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h4 class="card-title">Website Informations</h4>
                            <p class="card-category">Website information that show in landingpage</p>
                        </div>
                        <div class="">
                            <a class="btn-edit" href="javascript:void(0)" id="editInformation">
                                <i class="material-icons">edit</i> Edit
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Logo Primary</th>
                            <td>
                                <a class="link-image" data-fancybox="gallery" href="{{ asset(getLogo()) }}">
                                    <img
                                        src="{{ asset(getLogo()) }}"
                                        class="shadow-1-strong rounded" height="50px"
                                        alt=""
                                    />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th>Logo Secondary</th>
                            <td>
                                <a class="link-image" data-fancybox="gallery" href="{{ asset(getLogoSec()) }}">
                                    <img
                                        src="{{ asset(getLogoSec()) }}"
                                        class="shadow-1-strong rounded" height="50px"
                                        alt=""
                                    />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th rowspan="3">Social</th>
                            <td>
                                <a href="{{ getSocialsLink($info)['linkedin'] }}">
                                    LinkedIn : {{ getSocialsLink($info)['linkedin'] }}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ getSocialsLink($info)['dribble'] }}">
                                    Dribble : {{ getSocialsLink($info)['dribble'] }}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ getSocialsLink($info)['instagram'] }}">
                                    Instagram : {{ getSocialsLink($info)['instagram'] }}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th>Working Experience: Start Working</th>
                            <td>
                                {{ getExperience($info) }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-7" id="editSection" style="display: none">
            <div class="card">
                <div class="card-header card-header-tabs card-header-warning">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <span class="nav-tabs-title">Sections:</span>
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#logo" data-toggle="tab">
                                        <i class="material-icons">image</i> Logo
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#socials" data-toggle="tab">
                                        <i class="material-icons">groups</i> Socials
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#info" data-toggle="tab">
                                        <i class="material-icons">info</i> Other Info
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="formInformation" data-parsley-validate="" enctype="multipart/form-data">
                        <input type="hidden" id="idClient" name="idClient" value="1">
                        <div class="tab-content">
                            <div class="tab-pane active" id="logo">
                                <div class="form-group d-flex justify-content-center">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new img-thumbnail">
                                            <img src="https://via.placeholder.com/800x350/FFFFFF?text=No+Image"
                                                 alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail"
                                             style="max-width: 800px; max-height: 350px;"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-primary btn-simple btn-file">
                                               <span class="fileinput-new">Select Primary Logo</span>
                                               <span class="fileinput-exists">Change</span>
                                               <input type="file" id="logoPrimary" accept=".png" name="logoPrimary"
                                                      data-parsley-errors-container="#errorBlockPrimary"/>
                                            </span>
                                            <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                                               data-dismiss="fileinput">
                                                <i class="fa fa-times"></i> Remove</a>
                                            <div id="errorBlockPrimary"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group d-flex justify-content-center">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new img-thumbnail">
                                            <img src="https://via.placeholder.com/800x350/FFFFFF?text=No+image"
                                                 alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail"
                                             style="max-width: 800px; max-height: 350px;"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-primary btn-simple btn-file">
                                               <span class="fileinput-new">Select Secondary Logo</span>
                                               <span class="fileinput-exists">Change</span>
                                               <input type="file" id="logoSec" accept=".png" name="logoSec"
                                                      data-parsley-errors-container="#errorBlockSecondary"/>
                                            </span>
                                            <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                                               data-dismiss="fileinput">
                                                <i class="fa fa-times"></i> Remove</a>
                                            <div id="errorBlockSecondary"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="socials">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Url LinkedIn</label>
                                    <input class="form-control" type="text" name="linked_in" id="linkedIn"
                                           value="{{ getSocialsLink($info)['linkedin'] }}" data-parsley-linkedin="" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label class="bmd-label-floating">Url Dribble</label>
                                    <input class="form-control" type="text" name="dribble" id="dribble"
                                           value="{{ getSocialsLink($info)['dribble'] }}" data-parsley-dribble="" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label class="bmd-label-floating">Url Instagram</label>
                                    <input class="form-control" type="text" name="instagram" id="instagram"
                                           value="{{ getSocialsLink($info)['instagram'] }}" data-parsley-instagram="" autocomplete="off">
                                </div>
                            </div>
                            <div class="tab-pane" id="info">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Start Working</label>
                                    <input class="form-control" type="text" name="start_working" id="startWorking"
                                           value="{{ getExperience($info) }}" data-parsley-startWorking="" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between flex-row-reverse">
                            <button type="button" class="btn btn-primary pull-right" id="submitInformation">Submit</button>
                            <button type="button" class="btn btn-secondary pull-right" id="cancelEdit">Cancel</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        let Information = {
            CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),
            btnEdit: $('#editInformation'),
            btnCancel: $('#cancelEdit'),
            btnSubmit: $('#submitInformation'),
            infoSection: $('#infoSection'),
            editSection: $('#editSection'),
            editForm: $('#formInformation'),

            init: function() {
                Information.initBtnEdit();
                Information.initBtnCancel();
                Information.initBtnSubmit();
            },

            initBtnEdit: function () {
                Information.btnEdit.click(function (e) {
                    e.preventDefault();

                    Information.infoSection.removeClass('col-md-7').addClass('col-md-5');
                    Information.editSection.show();
                    $(this).attr('disabled', 'disabled');

                });
            },

            initBtnCancel: function () {
                Information.btnCancel.click(function (e) {
                    e.preventDefault();

                    Information.infoSection.removeClass('col-md-5').addClass('col-md-7');
                    Information.editSection.hide();
                    Information.btnEdit.removeAttr('disabled');

                });
            },

            initBtnSubmit: function () {
                Information.btnSubmit.click(function (e) {
                    e.preventDefault();

                    if (!Information.editForm.parsley().validate()) {
                        return;
                    }

                    $(this).prop('disabled', true).text('Saving ..');
                    let id = $('#idClient').val();
                    Information.doSubmit(id);
                });
            },

            doSubmit: function (id) {
                let url = '/admin/info/edit/' + id,
                    data = new FormData(Information.editForm[0]),
                    type = 'POST';

                Information.submitForm(url, data, type, Information.btnSubmit);
            },

            submitForm: function (url, data, type, btn) {

                return $.ajax({
                    headers: {'X-CSRF-TOKEN': Information.CSRF_TOKEN},
                    url : url,
                    type : type,
                    method: type,
                    data : data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data){
                        btn.prop('disabled', false).text('Submit');

                        swal.fire({
                            title: 'Success',
                            text: data.msg,
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Close'
                        }).then((result) => {
                            if(result){
                                // Do Stuff here for success
                                location.reload();
                            }
                        });
                    },
                    error: function (xhr, status, error){

                        swal.fire({
                            title: 'Failed!',
                            text: xhr.responseText,
                            icon: 'error',
                            confirmButtonText: 'Close'
                        });
                    }
                });
            }

        }

        window.addEventListener('load', Information.init);
    </script>
@endsection
