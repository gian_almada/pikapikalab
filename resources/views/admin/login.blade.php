@extends('admin.template.templateLogin')

@section('login')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-5">
                <img src="{{ asset('assets/landingpage/img/logo-new.png') }}" alt="" class="logo img-fluid img-header-login">
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="login-wrap p-0">
                    <h3 class="mb-4 text-center">Login Admin</h3>
                    @include('admin.template.flash-message')
                    <form class="signin-form" action="{{ url('/loginPost') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Username" name="username" required>
                        </div>
                        <div class="form-group">
                            <input id="password-field" type="password" class="form-control" name="password" placeholder="Password" required>
                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control btn btn-primary submit px-3">LOGIN</button>
                        </div>
                        <div class="form-group d-md-flex">
                            <div class="w-50"></div>
                            <div class="w-50 text-md-right">
                                <a href="{{ url('/forget-password') }}" style="color: #fff">Forgot Password</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
