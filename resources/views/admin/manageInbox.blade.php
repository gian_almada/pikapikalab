@extends('admin.template.templateAdmin')

@section('title', 'Inbox Page')

@section('header', 'Mail Box')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h4 class="card-title">Mail Box</h4>
                            <p class="card-category">Inbox</p>
                        </div>
                        <div class="">
                            <?php if(getUnreadInbox() > 0): ?>
                            <a class="btn-edit" href="javascript:void(0)" id="markAllasRead">
                                <i class="material-icons">mark_email_read</i> Mark all as read
                                <div class="ripple-container"></div>
                            </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>No.</th>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Subject</th>
                                <th class="text-center">Action</th>
                            </thead>
                            <tbody>
                            @if(count($mails) > 0)
                                @foreach($mails as $no => $inbox)
                                    <tr<?php if($inbox->read == 0): ?> class="unread"<?php endif ?>>
                                        <td>{{ $no+1 }}</td>
                                        <td>{{ \Carbon\Carbon::parse($inbox->created_at)->isoFormat('LLL') }}</td>
                                        <td>{{ $inbox->name }}</td>
                                        <td>{{ $inbox->email }}</td>
                                        <td>{{ $inbox->subject }}</td>
                                        <td class="text-center">
                                            <button type="button" rel="tooltip" title="Read"
                                                    class="btn btn-white<?php if($inbox->read == 0): ?> is-unread<?php endif; ?> btn-link btn-sm btn-read"
                                                    data-id="{{ $inbox->id }}"
                                                    data-name="{{ $inbox->name }}"
                                                    data-email="{{ $inbox->email }}"
                                                    data-subject="{{ $inbox->subject }}"
                                                    data-message="{{ $inbox->message }}">
                                                <i class="material-icons icon-unread"
                                                   style="display:<?php if($inbox->read == 0): ?>inline-block<?php else: ?>none<?php endif; ?>">
                                                    mark_email_unread
                                                </i>
                                                <i class="material-icons icon-read"
                                                   style="display:<?php if($inbox->read == 1): ?>inline-block<?php else: ?>none<?php endif; ?>">
                                                    mark_email_read
                                                </i>
                                            </button>

                                            <a rel="tooltip" title="Remove" class="btn btn-white btn-link btn-sm btn-delete"
                                               href="{{ url('/admin/Inbox/delete/' . $inbox->id) }}">
                                                <i class="material-icons">close</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">No data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        {{-- Pagination --}}
                        <div class="d-flex justify-content-center pagination">
                            {!! $mails->links('pagination::bootstrap-4') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5" id="readContainer" style="display: none">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h4 class="card-title" id="subject_mail"></h4>
                            <p class="card-category">From : <span id="mail_name"></span></p>
                        </div>
                        <div class="">
                            <a class="btn-edit" href="javascript:void(0)" id="closeReadContainer">
                                <i class="material-icons">close</i>
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    Message :
                    <div class="mt-3" id="mail_body"></div>
                </div>
            </div>
        </div>
    </div>

    <script>

        let Inbox = {
            CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),
            readContainer: $('#readContainer'),


            init: function () {
                Inbox.initBtnRead();
            },

            initBtnRead: function () {

                $('.btn-read').click(function (e) {
                    e.preventDefault();

                    let id = $(this).data('id'),
                        name = $(this).data('name'),
                        email = $(this).data('email'),
                        subject = $(this).data('subject'),
                        message = $(this).data('message');

                    Inbox.readContainer.show();

                    if ($(this).hasClass('is-unread')) {
                        $(this).removeClass('is-unread');
                        $(this).find('.icon-unread').hide();
                        $(this).find('.icon-read').css('display', 'inline-block');
                        $(this).closest('tr').removeClass('unread');
                    }

                    Inbox.doRead(id);
                    $('#subject_mail').html(subject);
                    $('#mail_name').html(name + ' ( <strong>' + email + '</strong> )');
                    $('#mail_body').html(message);
                });

                $('#markAllasRead').click(function (e) {
                    e.preventDefault();

                    $('.table tr').removeClass('unread');
                    $('.btn-read').removeClass('is-unread');
                    $('.icon-unread').hide();
                    $('.icon-read').css('display', 'inline-block');
                    Inbox.doRead('all');

                    $(this).hide();
                });

                $('#closeReadContainer').click(() => {
                    Inbox.readContainer.hide();
                });
            },

            doRead: function (id) {
                let url = '/admin/inbox/read/' + id,
                    type = 'GET';

                $.ajax({
                    headers: {'X-CSRF-TOKEN': Inbox.CSRF_TOKEN},
                    url: url,
                    type: type,
                    dataType: 'json', // added data type
                });
            },
        }

        Inbox.init();
    </script>
@endsection
