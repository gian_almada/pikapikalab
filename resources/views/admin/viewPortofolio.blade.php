@extends('admin.template.templateAdmin')

@section('title', 'Kategori Page')

@section('header', $kategori->name)

@section('content')
    <div class="row">
        <div class="col-12 mb-4">
            <a href="{{ url('admin/category') }}"><i class="material-icons">arrow_back</i> Back</a>
        </div>
    </div>
    <!-- Gallery -->
    <div class="row custom-flex">
        @foreach($porto as $key => $image)
            <div class="image-item">
                <a class="link-image" data-fancybox="gallery" href="{{ asset($porto[$key]['path'] . '/' . $porto[$key]['file_name']) }}">
                    <img
                        src="{{ asset($porto[$key]['path'] . '/' . $porto[$key]['file_name'])  }}"
                        class="shadow-1-strong rounded w-100" style="max-height: 30vh"
                        alt=""
                    />
                </a>
            </div>
        @endforeach
    </div>
    <!-- Gallery -->
@endsection
