@extends('admin.template.templateAdmin')

@section('title', 'Client Page')

@section('header', 'Clients')

@section('content')
    <div class="row">
        <div class="col-12 col-md-10">
            <div class="card card-upload-client">
                <div class="card-header card-header-primary">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h4 class="card-title">Client</h4>
                            <p class="card-category">List Client</p>
                        </div>
                        <div class="">
                            <a class="btn-upload" href="javascript:void(0)" id="showUploadClientForm">
                                <i class="material-icons">upload</i> Upload
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="formUpload" data-parsley-validate="" enctype="multipart/form-data">
                        <input type="hidden" id="idClient" name="idClient" value="">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 d-flex">
                                <div class="form-group justify-content-center">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new img-thumbnail" style="width: 100%;">
                                            <img src="https://via.placeholder.com/350/FFFFFF?text=No+Image" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 350px; max-height: 350px;"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-primary btn-simple btn-file">
                                               <span class="fileinput-new">Select Client Logo</span>
                                               <span class="fileinput-exists">Change</span>
                                               <input type="file" id="imageUpload" accept=".png" name="fileUpload" required data-parsley-errors-container="#errorBlock"/>
                                            </span>
                                            <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                                               data-dismiss="fileinput">
                                                <i class="fa fa-times"></i> Remove</a>
                                            <div id="errorBlock"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Client Name</label>
                                    <input class="form-control" type="text" name="clientName" id="clientName" required data-parsley-clientName=""  autocomplete="off">
                                </div>
                                <div class="form-group" id="previewLogoBefore" style="display: none">
                                    <label>Client Old Logo/Icon</label>
                                    <div class="fileinput-new img-thumbnail" style="height: auto; width: max-content; padding: 5px">
                                        <img id="imgSrc" src="https://via.placeholder.com/350x350/FFFFFF?text=No+Image" alt="..." style="height: auto; max-width: 350px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between flex-row-reverse">
                            <button type="button" class="btn btn-primary pull-right" id="submit">Upload</button>
                            <div>
                                <button type="button" class="btn btn-primary pull-right" id="edit" style="display: none">Edit</button>
                                <button type="button" class="btn btn-warning pull-right" id="cancelEdit" style="display: none">Reset Form</button>
                            </div>
                            <button type="button" class="btn btn-secondary pull-right"
                                    onclick="return $('.card-upload-client').toggleClass('show-form')">
                                Cancel
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-10">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Client</h4>
                    <p class="card-category">List Client</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                            <th>No.</th>
                            <th>Icon / Logo</th>
                            <th>Client Name</th>
                            <th class="text-center">Action</th>
                            </thead>
                            <tbody>
                            @if(count($clients) > 0)
                                @foreach($clients as $no => $client)
                                    <tr>
                                        <td>{{ $no+1 }}</td>
                                        <td>
                                            <a class="link-image" data-fancybox="gallery" href="{{ asset($client->path . '/' . $client->file_name) }}">
                                                <img
                                                    src="{{ asset($client->path . '/' . $client->file_name) }}"
                                                    class="shadow-1-strong rounded" height="50px"
                                                    alt=""
                                                />
                                            </a>
                                        </td>
                                        <td>{{ $client->clientName }}</td>
                                        <td class="text-center" style="width: 130px">
                                            <button type="button" rel="tooltip" title="Edit Client" class="btn btn-white btn-link btn-sm btn-edit" data-id="{{ $client->id }}" data-name="{{ $client->clientName }}" data-src="{{ asset($client->path . '/' . $client->file_name) }}">
                                                <i class="material-icons">edit</i>
                                            </button>
                                            <a rel="tooltip" title="Remove" class="btn btn-white btn-link btn-sm btn-delete" href="{{ url('/admin/client/delete/' . $client->id) }}">
                                                <i class="material-icons">close</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">No data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        {{-- Pagination --}}
                        <div class="d-flex justify-content-center pagination">
                            {!! $clients->links('pagination::bootstrap-4') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        let Client = {
            CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),
            formUploadPorto: $('#formUpload'),
            btnSubmit: $('#submit'),

            init: function () {
                Client.initBtnSubmit();
                Client.initBtnEdit();
                Client.initDeleteLink();
                Client.eventListener();
            },

            eventListener: function () {
                let toggleShowForm = $('#showUploadClientForm'),
                    uploadFormWrapper = $('.card-upload-client');

                toggleShowForm.click(function () {
                    uploadFormWrapper.toggleClass('show-form');
                });
            },

            initBtnEdit: function () {

                $('.btn-edit').click(function (e){
                    e.preventDefault();

                    let id = $(this).data('id'),
                        name = $(this).data('name'),
                        src  = $(this).data('src'),
                        previewLogoBefore = $('#previewLogoBefore'),
                        clientIdField = $('#idClient'),
                        clientNameField = $('#clientName'),
                        clientLogo = $('#imgSrc'),
                        btnCancel = $('#cancelEdit'),
                        btnEdit = $('#edit');

                    Client.btnSubmit.hide();
                    previewLogoBefore.show();
                    btnCancel.show();
                    btnEdit.show();
                    $('.fileinput-exists').trigger('click');

                    clientIdField.val(id);
                    clientNameField.val(name).parent().addClass('is-filled');
                    clientLogo.attr('src',src);

                    $('#imageUpload').removeAttr('required');
                    $('#clientName').removeAttr('required');
                });

                $('#cancelEdit').click(function (e) {
                    e.preventDefault();

                    let previewLogoBefore = $('#previewLogoBefore'),
                        clientIdField = $('#idClient'),
                        clientNameField = $('#clientName'),
                        btnCancel = $('#cancelEdit'),
                        btnEdit = $('#edit');

                    clientIdField.val('');
                    clientNameField.val('').parent().removeClass('is-filled');

                    Client.btnSubmit.show();
                    previewLogoBefore.hide();
                    btnCancel.hide();
                    btnEdit.hide();
                    $('.fileinput-exists').trigger('click');

                    $('#imageUpload').attr('required', true);
                    $('#clientName').attr('required', true);
                });

                $('#edit').click(function (e) {
                    e.preventDefault();

                    if (!Client.formUploadPorto.parsley().validate()) {
                        return;
                    }

                    $(this).prop('disabled', true).text('Saving ..');
                    let id = $('#idClient').val();
                    Client.doEdit(id, $(this));
                });
            },

            initBtnSubmit: function () {
                Client.btnSubmit.on('click', function (e){

                    e.preventDefault();

                    if (!Client.formUploadPorto.parsley().validate()) {
                        return;
                    }

                    $(this).prop('disabled', true).text('Saving ..');
                    Client.doSubmit();
                });
            },

            initDeleteLink: function () {
                $('.btn-delete').click(function (e) {
                    e.preventDefault();

                    let url = $(this).attr('href'),
                        getParams = Client.getUrlParameter('page');

                    swal.fire({
                        title: 'Are you sure?',
                        text: 'Once deleted, you will not be able to recover this data!',
                        icon: 'warning',
                        confirmButtonColor: '#ff9800',
                        confirmButtonText: 'Delete',
                        showCancelButton: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.href = url + (getParams ? '?page=' + getParams : '');
                        } else {
                            swal.fire('Your data is safe!', '', 'info');
                        }
                    });
                });
            },

            doSubmit: function () {
                let url = '/admin/client/add',
                    data = new FormData(Client.formUploadPorto[0]),
                    type = 'POST';

                Client.submitForm(url, data, type, Client.btnSubmit, 'add');
            },

            doEdit: function (id, btnEdit) {
                let url = '/admin/client/edit/' + id,
                    data = new FormData(Client.formUploadPorto[0]),
                    type = 'POST';

                Client.submitForm(url, data, type, btnEdit, 'edit');
            },

            submitForm: function (url, data, type, btn, action) {

                return $.ajax({
                    headers: {'X-CSRF-TOKEN': Client.CSRF_TOKEN},
                    url : url,
                    type : type,
                    method: type,
                    data : data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data){
                        if (action == 'edit') {
                            btn.prop('disabled', false).text('Submit');
                        } else {
                            btn.prop('disabled', false).text('Edit');
                        }

                        swal.fire({
                            title: 'Success',
                            text: data.msg,
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Close'
                        }).then((result) => {
                            if(result){
                                // Do Stuff here for success
                                location.reload();
                            }
                        });
                    },
                    error: function (xhr, status, error){

                        swal.fire({
                            title: 'Failed!',
                            text: xhr.responseText,
                            icon: 'error',
                            confirmButtonText: 'Close'
                        });
                    }
                });
            },

            getUrlParameter: function (sParam) {
                let sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
                return false;
            }
        }

        Client.init();
    </script>
@endsection
