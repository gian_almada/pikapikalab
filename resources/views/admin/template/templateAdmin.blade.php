<!--
=========================================================
* Material Dashboard Dark Edition - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-dark
* Copyright 2019 Creative Tim (http://www.creative-tim.com)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.template.layout.head')
</head>

<body class="dark-edition" style="overflow: hidden">
<div class="wrapper ">

    @include('admin.template.layout.sidebar')

    <div class="main-panel">

        @include('admin.template.layout.nav')

        <div class="content">
            <div class="container-fluid">
                @include('admin.template.flash-message')

                @yield('content')
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright float-right" id="date">
                    , Pikapika Lab.
                </div>
            </div>
        </footer>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="modalChangePassword" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <form id="formChangePassword" data-parsley-validate="">
                <input type="hidden" id="id_user" value="{{ Session::get('id') }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="bmd-label-floating" for="oldPassword">
                                    Old Password
                                </label>
                                <input type="text" name="oldPassword" id="oldPassword" required
                                       data-parsley-oldpassword=""
                                       data-parsley-trigger="focusout"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="bmd-label-floating" for="newPassword">
                                    New Password
                                </label>
                                <input type="text" name="newPassword" id="newPassword" data-parsley-type="alphanum"
                                       data-parsley-minlength="7"
                                       data-parsley-trigger="focusout"
                                       data-parsley-minlength-message="Password must be at least 7 characters" required
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="bmd-label-floating" for="confirmNewPassword">
                                    Confirm New Password
                                </label>
                                <input type="text" name="color" name="confirmNewPassword" id="confirmNewPassword" data-parsley-type="alphanum"
                                       data-parsley-minlength="7"
                                       data-parsley-trigger="blur focusout"
                                       data-parsley-equalto="#newPassword"
                                       data-parsley-equalto-message="Confirmation password must be same as new password"
                                       required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" id="submitChangePassword" disabled>Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@include('admin.template.layout.scriptLoad')

</body>
</html>
