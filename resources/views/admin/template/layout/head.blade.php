<meta charset="utf-8"/>
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/assets/adminpage/img/apple-icon.png')}}">
<link rel="icon" type="image/png" href="{{ asset('/favicon.png')}}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>
    Pikapika Lab - Admin | @yield('title')
</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
      name='viewport'/>
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- CSS Files -->
<link href="{{ asset('/assets/adminpage/css/material-dashboard.css?v=2.1.0') }}" rel="stylesheet"/>
<link href="{{ asset('assets/landingpage/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
<!-- CSS Just for demo purpose, don't include it in your project -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<link href="{{ asset('/assets/adminpage/demo/demo.css') }}" rel="stylesheet"/>
<link href="{{ asset('/css/app.css') }}" rel="stylesheet"/>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"/>
<link rel="stylesheet" href="{{ asset('/assets/adminpage/js/plugins/vanilla-picker/dist/vanilla-picker.csp.css') }}"/>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
