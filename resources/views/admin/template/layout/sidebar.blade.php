<div class="sidebar" data-color="purple" data-background-color="black" data-image="{{ asset('/assets/adminpage/img/sidebar-2.jpg')}}">

    <?php $path = getLogoSec(); ?>
    <div class="logo">
        <a href="{{ url('/admin/dashboard') }}" class="simple-text logo-normal">
            <img src="{{ asset($path) }}" alt="" class="img-fluid logo-nav" height="30">
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item{{ (request()->is('admin/dashboard')) ? ' active' : '' }} ">
                <a class="nav-link" href="{{ url('/admin/dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item{{ (request()->is('admin/category')) || (request()->is('admin/category/*'))? ' active' : '' }}">
                <a class="nav-link" href="{{ url('/admin/category') }}">
                    <i class="material-icons">apps</i>
                    <p>Kategori</p>
                </a>
            </li>
            <li class="nav-item{{ (request()->is('admin/portofolio')) ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('/admin/portofolio') }}">
                    <i class="material-icons">collections</i>
                    <p>Portofolio</p>
                </a>
            </li>
            <li class="nav-item{{ (request()->is('admin/client')) ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('/admin/client') }}">
                    <i class="material-icons">recommend</i>
                    <p>Client</p>
                </a>
            </li>
            <li class="nav-item{{ (request()->is('admin/info')) ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('/admin/info') }}">
                    <i class="material-icons">assignment</i>
                    <p>Manage Info</p>
                </a>
            </li>
            <li class="nav-item{{ (request()->is('admin/inbox')) ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('/admin/inbox') }}">
                    <i class="material-icons">inbox</i>
                    <div class="d-flex justify-content-between align-items-center">
                        <p>Inbox</p>
                        <?php
                            $cnt = getUnreadInbox();
                            if ($cnt > 0 && !request()->is('admin/inbox')): ?>
                            <div>
                                <h5 class="mb-0"><span class="badge badge-pill badge-success">{{ getUnreadInbox() }}</span></h5>
                            </div>
                        <?php endif; ?>
                    </div>
                </a>
            </li>

            <!-- view landing page  -->
            <li class="nav-item view-landingpage">
                <a class="nav-link" href="{{ url('/') }}" target="_blank">
                    <i class="material-icons">open_in_new</i>
                    <p>View Landing Page</p>
                </a>
            </li>
        </ul>
    </div>
</div>
