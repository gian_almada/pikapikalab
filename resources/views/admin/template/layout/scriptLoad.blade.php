<!--   Core JS Files   -->
<script src="{{ asset('/assets/adminpage/js/core/popper.min.js') }}"></script>
<script src="{{ asset('/assets/adminpage/js/core/bootstrap-material-design.min.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/assets/adminpage/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.7.0/parsley.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Chartist JS -->
<script src="{{ asset('/assets/adminpage/js/plugins/chartist.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('/assets/adminpage/js/plugins/bootstrap-notify.js') }}"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('/assets/adminpage/js/material-dashboard.js?v=2.1.0') }}"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('/assets/adminpage/demo/demo.js') }}"></script>
<!-- CUSTOM JS -->
<script src="{{ asset('/js/kategori.js') }}"></script>
<script src="{{ asset('/js/jquery.masonryGrid.js') }}"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js"></script>=
<script src="{{ asset('/assets/adminpage/js/plugins/vanilla-picker/dist/vanilla-picker.csp.min.js') }}"></script>=

<script>
    const x = new Date().getFullYear();
    let date = document.getElementById('date');
    date.innerHTML = '&copy; ' + x + date.innerHTML;

    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();
    });
</script>
