@extends('admin.template.templateAdmin')

@section('title', 'Portofolio Page')

@section('header', 'Portofolio')

@section('content')
    <div class="row">
        <div class="col-12 col-md-10">
            <div class="card card-upload-porto">
                <div class="card-header card-header-primary">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h4 class="card-title">Portofolio</h4>
                            <p class="card-category">Upload your portofolio</p>
                        </div>
                        <div class="">
                            <a class="btn-edit" href="javascript:void(0)" id="showUplaoadForm">
                                <i class="material-icons">upload</i> Upload
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="formUpload" data-parsley-validate="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-12 col-md-6 d-flex flex-column">
                                <div class="form-group justify-content-center">
                                    <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                        <div class="fileinput-new img-thumbnail">
                                            <img src="https://via.placeholder.com/600x350/FFFFFF?text=No+Image" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 600px; max-height: 350px;"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-primary btn-simple btn-file">
                                               <span class="fileinput-new">Select image</span>
                                               <span class="fileinput-exists">Change</span>
                                               <input type="file" id="imageUpload" name="fileUpload" required data-parsley-errors-container="#errorBlock"/>
                                            </span>
                                            <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                                               data-dismiss="fileinput">
                                                <i class="fa fa-times"></i> Remove</a>
                                            <div id="errorBlock"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mt-3">
                                <div class="form-group">
                                    <label for="konten">Description</label>
                                    <div id="area" class="pt-3">
                                        <textarea id="konten" class="form-control w-100" name="deskripsi" rows="10" cols="50"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Category</label>
                                    <select class="form-control selectpicker pt-2" data-style="btn btn-link" required
                                            id="kategori" name="kategori">
                                        @foreach($kategoriList as $kategori)
                                            <option value="{{ $kategori->id }}">{{ $kategori->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Client</label>
                                    <select class="form-control selectpicker pt-2" data-style="btn btn-link" required
                                            id="client" name="client">
                                        <option value="0"> - </option>
                                        @foreach($clientList as $client)
                                            <option value="{{ $client->id }}">{{ $client->clientName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between flex-row-reverse">
                            <button type="button" class="btn btn-primary pull-right" id="submit">Upload</button>
                            <button type="button" class="btn btn-secondary pull-right"
                                    onclick="return $('.card-upload-porto').toggleClass('show-form')">
                                Cancel
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-10">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Portofolio</h4>
                    <p class="card-category">List Portofolio</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                            <th>Image</th>
                            <th>Kategori</th>
                            <th>Client</th>
                            <th class="text-center">Action</th>
                            </thead>
                            <tbody>
                            @if(count($portofolioList) > 0)
                                @foreach($portofolioList as $porto)
                                    <tr>
                                        <td>
                                            <a class="link-image" data-fancybox="gallery" href="{{ asset($porto->path . '/' . $porto->file_name) }}">
                                                <img
                                                    src="{{ asset($porto->path . '/' . $porto->file_name) }}"
                                                    class="shadow-1-strong rounded" height="50px"
                                                    alt=""
                                                />
                                            </a>
                                        </td>
                                        <td>{{ getKategoriName($porto->kategori) }}</td>
                                        <td>{{ getClientName($porto->client_id) }}</td>
                                        <td class="text-center" style="width: 130px">
                                        <?php
                                            $title = 'Display';
                                            $class = 'warning';
                                            $icon = 'visibility_off';

                                            if ($porto->display == 1) {
                                                $title = 'Hide';
                                                $class = 'info';
                                                $icon = 'visibility';
                                            }
                                        ?>
                                            <button type="button" rel="tooltip" title="{{ $title }} Portofolio"
                                                    class="btn btn-{{ $class }} btn-link btn-sm btnDisplay"
                                                    data-id="{{ $porto->id }}" data-display="{{ $porto->display }}">
                                                <i class="material-icons">{{ $icon }}</i>
                                            </button>
                                            <button type="button" rel="tooltip" title="Edit Porto"
                                                    class="btn btn-white btn-link btn-sm btn-edit"
                                                    data-toggle="modal" data-target="#modalEditPorto"
                                                    data-id="{{ $porto->id }}"
                                                    data-kategori="{{ $porto->kategori }}"
                                                    data-client="{{ $porto->client_id }}"
                                                    data-desc="{{ $porto->deskripsi }}">
                                                <i class="material-icons">edit</i>
                                            </button>
                                            <a rel="tooltip" title="Remove"
                                               class="btn btn-white btn-link btn-sm btn-delete"
                                               href="{{ url('/admin/portofolio/delete/' . $porto->id) }}">
                                                <i class="material-icons">close</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">No data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        {{-- Pagination --}}
                        <div class="d-flex justify-content-center align-items-center pagination flex-column">
                            <p>
                                Displaying {{$portofolioList->count()}} of {{ $portofolioList->total() }} item(s).
                            </p>
                            {!! $portofolioList->links('pagination::bootstrap-4') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="modalEditPorto" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Portofolio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="material-icons">clear</i>
                    </button>
                </div>
                <form id="formEditPorto" data-parsley-validate="">
                    <input name="idEdit" id="editId" value="" type="hidden">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Kategori</label>
                                    <select class="form-control selectpicker mt-3" data-style="btn btn-link" required
                                            id="kategoriEdit" name="kategori">
                                        @foreach($kategoriList as $kategori)
                                            <option value="{{ $kategori->id }}">{{ $kategori->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Client</label>
                                    <select class="form-control selectpicker mt-3" data-style="btn btn-link" required
                                            id="clientEdit" name="client">
                                        <option value="0"> - </option>
                                        @foreach($clientList as $client)
                                            <option value="{{ $client->id }}">{{ $client->clientName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-3">
                                <div class="form-group">
                                    <label for="kontenEdit">Description</label>
                                    <div id="area" class="pt-3">
                                        <textarea id="kontenEdit" class="form-control w-100" name="deskripsi" rows="10" cols="50"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-right" id="submitEditPorto">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        var konten = document.getElementById('konten');
        var kontenEdit = document.getElementById('kontenEdit');
        CKEDITOR.replace(konten,{
            language:'en-gb',
            on : {
                // maximize the editor on startup
                'instanceReady': function (evt) {
                    evt.editor.resize("100%", $("#area").height());
                }
            }
        });

        CKEDITOR.replace(kontenEdit,{
            language:'en-gb',
            on : {
                // maximize the editor on startup
                'instanceReady': function (evt) {
                    evt.editor.resize("100%", $("#area").height());
                }
            }
        });

        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.width = '100%';
    </script>

    <script>

        let Portofolio = {
            CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),
            formUploadPorto: $('#formUpload'),
            formEditPorto: $('#formEditPorto'),
            btnSubmit: $('#submit'),
            editModal: $('#modalEditPorto'),

            init: function () {
                Portofolio.initModalShow();
                Portofolio.initBtnSubmit();
                Portofolio.initBtnEdit();
                Portofolio.initDisplay();
                Portofolio.initDeleteLink();

                Portofolio.eventListener();
            },

            eventListener: function () {
              let toggleShowForm = $('#showUplaoadForm'),
                  uploadFormWrapper = $('.card-upload-porto');

                toggleShowForm.click(function () {
                    uploadFormWrapper.toggleClass('show-form');
                });
            },

            updateAllMessageForms: function () {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            },

            initModalShow: function () {

                Portofolio.editModal.on('show.bs.modal', function (){
                    let btnEdit = $('#submitEditPorto');

                    const doSubmit = function (){
                        let url = '/admin/portofolio/edit/' + $('input[name="idEdit"]').val(),
                            data = new FormData(Portofolio.formEditPorto[0]);

                        btnEdit.prop('disabled', true).text('Saving ..');

                        Portofolio.submitForm(url, data, 'POST', btnEdit, 'edit');
                    }

                    btnEdit.click(function (e){

                        e.preventDefault();

                        if (!Portofolio.formEditPorto.parsley().validate()) {
                            return;
                        }

                        Portofolio.updateAllMessageForms();

                        doSubmit();
                    });
                });
            },

            initBtnSubmit: function () {
                Portofolio.btnSubmit.on('click', function (e){

                    e.preventDefault();

                    if (!Portofolio.formUploadPorto.parsley().validate()) {
                        return;
                    }

                    Portofolio.updateAllMessageForms();

                    $(this).prop('disabled', true).text('Saving ..');
                    Portofolio.doSubmit();
                });
            },

            initBtnEdit: function () {
                $('.btn-edit').click(function (e){
                    e.preventDefault();

                    let id = $(this).data('id'),
                        category = $(this).data('kategori'),
                        client = $(this).data('client'),
                        desc   = $(this).data('desc');

                    $('#editId').val(id);
                    $('#kategoriEdit').val(category).change();
                    $('#clientEdit').val(client).change();
                    CKEDITOR.instances['kontenEdit'].setData(desc);

                });
            },

            initDisplay: function () {
                $('.btnDisplay').click(function (e){
                    e.preventDefault();
                    $(this).attr('disabled', true);

                    let id = $(this).data('id'),
                        display = $(this).data('display')

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': Kategori.CSRF_TOKEN},
                        url : '/admin/portofolio/display',
                        type : 'POST',
                        data : {
                            id: id,
                            display: display == 1 ? 0 : 1
                        },
                        dataType:'json',
                        success: function (){
                            swal.fire({
                                title: 'Success',
                                text: 'Success change status diplay.',
                                icon: 'success',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Close'
                            }).then((result) => {
                                if(result){
                                    // Do Stuff here for success
                                    location.reload();
                                }
                            });
                        }
                    });
                });
            },

            initDeleteLink: function () {
                $('.btn-delete').click(function (e) {
                    e.preventDefault();

                    let url = $(this).attr('href'),
                        getParams = Portofolio.getUrlParameter('page');

                    swal.fire({
                        title: 'Are you sure?',
                        text: 'Once deleted, you will not be able to recover this data!',
                        icon: 'warning',
                        confirmButtonColor: '#ff9800',
                        confirmButtonText: 'Delete',
                        showCancelButton: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.href = url + (getParams ? '?page=' + getParams : '');
                        } else {
                            swal.fire('Your data is safe!', '', 'info');
                        }
                    });
                });
            },

            doSubmit: function () {
                let url = '/admin/portofolio/add',
                    data = new FormData(Portofolio.formUploadPorto[0]),
                    type = 'POST';

                Portofolio.submitForm(url, data, type, Portofolio.btnSubmit, 'add');
            },

            submitForm: function (url, data, type, btn, action) {

                return $.ajax({
                    headers: {'X-CSRF-TOKEN': Portofolio.CSRF_TOKEN},
                    url : url,
                    type : type,
                    method: type,
                    data : data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data){
                        btn.prop('disabled', false).text('Submit');

                        if (action == 'edit') {
                            Portofolio.editModal.hide();
                        }

                        swal.fire({
                            title: 'Success',
                            text: data.msg,
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Close'
                        }).then((result) => {
                            if(result){
                                // Do Stuff here for success
                                location.reload();
                            }
                        });
                    },
                    error: function (xhr, status, error){

                        swal.fire({
                            title: 'Failed!',
                            text: xhr.responseText,
                            icon: 'error',
                            confirmButtonText: 'Close'
                        });
                    }
                });
            },

            getUrlParameter: function (sParam) {
                let sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
                return false;
            }
        }

        Portofolio.init();

        $(document).ready(function() {
            $('.selectpicker').selectpicker();
        })

    </script>
@endsection
