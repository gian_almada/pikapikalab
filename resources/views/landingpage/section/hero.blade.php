<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
        <div class="row justify-content-left">
            <div class="col-xl-9 col-lg-9 text-left">
                <div><span class="labIcon"></span>
                    <h1>Hello</h1></div>
                <h2 class="monserrat" data-aos="fade-up" data-aos-delay="200">My name is Priska Winarni</h2>
                <h3 class="monserrat" data-aos="fade-up" data-aos-delay="300">I'm Graphic Designer & Illustrator</h3>
                <h4 class="monserrat" data-aos="fade-up" data-aos-delay="400">Heyhoo everyone, you can call me priska <span>aka</span> pikapika.<br>And this is
                    my <strong>portofolio</strong></h4>
            </div>
        </div>
        <div class="btn-porto" data-aos="fade-up" data-aos-delay="500">
            <a href="#portfolio" class="btn-get-started scrollto">See my portofolio</a>
        </div>
    </div>
</section><!-- End Hero -->
