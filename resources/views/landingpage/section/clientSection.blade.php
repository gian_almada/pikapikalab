<!-- ======= Clients Section ======= -->
<section id="clients" class="clients section-bg">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Clients</h2>
        </div>

        <div class="row d-flex justify-content-center">
           <div class="main-carousel">
               @foreach($clients as $client)
                   <div class="carousel-cell">
                       <img src="{{ url( $client->path . '/' . $client->file_name )}}" class="img-fluid" alt="">
                   </div>
               @endforeach
           </div>
        </div>
    </div>
    <script>
        $(function () {
            $('.main-carousel').flickity({
                // options
                cellAlign: 'center',
                wrapAround: true,
                autoPlay: true
            });
        });
    </script>
</section><!-- End Clients Section -->
