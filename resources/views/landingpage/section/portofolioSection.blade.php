<!-- ======= Services Section ======= -->
<section id="portfolio" class="services section-bg">
    <div class="container" >

        <div class="section-title" data-aos="fade-up" data-aos-delay="100">
            <h2>Categories</h2>
        </div>

        <div class="row">
            <?php
                $iterate = 1;
            ?>
                @foreach($categories as $key => $kategori)
                    @if($kategori->display == 1)
                        <div class="box-wrapper col-lg-4 col-md-6 d-flex align-items-stretch<?php if ($key != 0 && $key != 1 && $key != 2) : ?> mt-4<?php elseif($key == 1): ?> mt-4 mt-md-0<?php elseif($key == 2): ?> mt-4 mt-lg-0<?php endif; ?>"
                             data-aos="zoom-in" data-aos-delay="{{ ($iterate * 100) + 100 }}">
                            <a href="{{ url('/portofolio/' . $kategori->alias) }}">
                                <div class="icon-box w-100"
                                     data-color="{{$kategori->color ? $kategori->color : '007bff'}}">
                                    <div class="icon">
                                        <i class="bx {{ $kategori->thumbnail ? $kategori->thumbnail : 'bx-loader-circle' }} bx-tada"
                                           style="color: {{ '#' . $kategori->color}}"></i>
                                    </div>
                                    <h4>{{ $kategori->name }}</h4>
                                    <p>{{ mb_strimwidth($kategori->deskripsi, 0, 100, " ...") }}</p>
                                </div>
                            </a>
                        </div>
                        <?php
                        $iterate = ($iterate == 3) ? 1 : $iterate + 1;
                        ?>
                    @endif
                @endforeach

        </div>

    </div>
</section><!-- End Sevices Section -->
<script>
    $('.icon-box').hover(
        function(){
            let color = $(this).data('color');
            $(this).css('backgroundColor','#'+color);
            $(this).find('.bx').css('color','#fff');
        },
        function(){
            let color = $(this).data('color');
            $(this).css('backgroundColor','#FFF');
            $(this).find('.bx').css('color','#'+color);
        });
</script>
