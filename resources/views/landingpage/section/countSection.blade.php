<!-- ======= Counts Section ======= -->
<section id="counts" class="counts section-bg">
    <div class="container">

        <div class="row justify-content-end">

            <div class="col-lg-4 col-md-4 col-4 d-md-flex align-items-md-stretch">
                <div class="count-box">
                    <span data-toggle="counter-up">{{ getCountClient() }}</span>
                    <p>Clients</p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-4 d-md-flex align-items-md-stretch">
                <div class="count-box">
                    <span data-toggle="counter-up">{{ getCountPortofolio() }}</span>
                    <p>Projects</p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-4 d-md-flex align-items-md-stretch">
                <div class="count-box">
                    <span data-toggle="counter-up">{{ date('Y') - getExperience($info) }}</span>
                    <p>Years of experience</p>
                </div>
            </div>

        </div>

    </div>
</section><!-- End Counts Section -->
