<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Pikapikalab | @yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="{{ asset('/favicon.png')}}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=https://fonts.googleapis.com/css?family=Inconsolata:400,500,600,700|Raleway:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons|Lexend:300,400,700"/>

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/portofolio/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/portofolio/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/landingpage/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/landingpage/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/landingpage/vendor/remixicon/remixicon.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('css/landingpage-bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/portofolio/css/style.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"/>

    <script src="{{ asset('assets/landingpage/vendor/jquery/jquery.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.7.0/parsley.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- =======================================================
    * Template Name: MyPortfolio - v4.5.0
    * Template URL: https://bootstrapmade.com/myportfolio-bootstrap-portfolio-website-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

@include('landingpage.portofolio.headerPortofolio')

<main id="main">

    @yield('content')

</main><!-- End #main -->

@include('landingpage.portofolio.footerPortofolio')

<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Contact me</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formContact" data-parsley-validate="">
                <div class="modal-body mx-3">
                    <div class="md-form mb-2">
                        <label data-error="wrong" data-success="right" for="form34"><i class='bx bx-user' ></i> Your name</label>
                        <input type="text" id="clientName" name="name" class="form-control validate" required
                               data-parsley-name=""
                               data-parsley-trigger="focusout">
                    </div>

                    <div class="md-form mb-2">
                        <label data-error="wrong" data-success="right" for="form29"><i class='bx bx-envelope' ></i> Your email</label>
                        <input type="email" id="clientEmail" name="email" class="form-control validate" required
                               data-parsley-email=""
                               data-parsley-type="email"
                               data-parsley-trigger="focusout">
                    </div>

                    <div class="md-form mb-2">
                        <label data-error="wrong" data-success="right" for="form32"><i class='bx bx-tag' ></i> Subject</label>
                        <input type="text" id="clientSubject" name="subject" class="form-control validate" required
                               data-parsley-subject=""
                               data-parsley-trigger="focusout">
                    </div>

                    <div class="md-form">
                        <label data-error="wrong" data-success="right" for="form8"><i class='bx bx-message-detail text-gray prefix'></i> Your message</label>
                        <textarea type="text" id="clientMessage" name="message" class="md-textarea form-control" rows="4" required
                                  data-parsley-message=""
                                  data-parsley-trigger="focusout"></textarea>
                    </div>

                </div>
                <div class="modal-footer d-flex justify-content-end">
                    <button type="button" id="submitEmail" class="btn btn-custom btn-unique">Send <i class='bx bxs-paper-plane'></i></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Vendor JS Files -->
<script src="{{ asset('assets/landingpage/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/counterup/counterup.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/aos/aos.js') }}"></script>

<script src="{{ asset('assets/portofolio/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script src="{{ asset('assets/portofolio/vendor/php-email-form/validate.js')}}"></script>
<script src="{{ asset('assets/portofolio/vendor/swiper/swiper-bundle.min.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/landingpage/js/main.js')}}"></script>
<script src="{{ asset('assets/portofolio/js/main.js')}}"></script>

<script>
    $('[data-fancybox]').fancybox({
        width       : 30%,
        height      : 100%,
        aspectRatio : true,
    })
</script>

<script>
    let Contact = {
        CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),
        formContact: $('#formContact'),
        btnSubmit: $('#submitEmail'),
        modalContact: $('#modalContactForm'),

        init: function () {
            Contact.initBtnSubmit();
        },

        initBtnSubmit: function () {
            Contact.btnSubmit.click(function (e) {
                e.preventDefault();

                if (!Contact.formContact.parsley().validate()) {
                    return;
                }

                $(this).prop('disabled', true).text('Saving ..');
                Contact.doSubmit();
            });
        },

        doSubmit: function () {
            let url = '/contact/send-email',
                data = new FormData(Contact.formContact[0]),
                type = 'POST';

            Contact.submitForm(url, data, type);
        },

        submitForm: function (url, data, type) {

            return $.ajax({
                headers: {'X-CSRF-TOKEN': Contact.CSRF_TOKEN},
                url : url,
                type : type,
                method: type,
                data : data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data){
                    Contact.modalContact.modal('hide');

                    if (data.status == true) {
                        console.log('test')
                        swal.fire({
                            title: 'Success',
                            text: data.msg,
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Close'
                        }).then((result) => {
                            if(result){
                                Contact.formContact.parsley().reset();
                                Contact.formContact.trigger('reset');
                            }
                        });
                    } else {
                        swal.fire({
                            title: 'Failed!',
                            text: data.msg,
                            icon: 'error',
                            confirmButtonText: 'Close'
                        });
                    }
                },
                error: function (xhr, status, error){

                    swal.fire({
                        title: 'Failed!',
                        text: xhr.responseText,
                        icon: 'error',
                        confirmButtonText: 'Close'
                    });
                }
            });
        }

    }

    window.addEventListener('load', Contact.init);
</script>

</body>

</html>
