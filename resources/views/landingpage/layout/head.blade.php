<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>Pikapika Lab</title>
<meta content="" name="description">
<meta content="" name="keywords">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- Favicons -->
<link rel="icon" type="image/png" href="{{ asset('/favicon.png')}}">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons|Lexend:300,400,700" rel="stylesheet" type="text/css" />
<!-- Vendor CSS Files FONT -->
<link href="{{ asset('assets/landingpage/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/landingpage/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/landingpage/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
<link href="{{ asset('assets/landingpage/vendor/flickity/dist/flickity.css') }}" rel="stylesheet" media="screen">

<!-- Template Main CSS File -->
<link href="{{ asset('css/landingpage-bundle.css') }}" rel="stylesheet">

<script src="{{ asset('assets/landingpage/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/flickity/dist/flickity.pkgd.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.7.0/parsley.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
