<!-- Vendor JS Files -->
<script src="{{ asset('assets/landingpage/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/counterup/counterup.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/landingpage/vendor/aos/aos.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/landingpage/js/main.js') }}"></script>
