<!-- ======= Footer ======= -->
<footer class="footer" role="contentinfo" id="footer">
    <div class="container d-md-flex py-4">
        <div class="mr-md-auto text-center text-md-left col-12 col-md-6">
            <div class="copyright">
                &copy; Copyright <strong><span>OnePage</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/onepage-multipurpose-bootstrap-template/ -->
                Designed by <a href="{{ url('/') }}">Pikapika Lab</a>
            </div>
        </div>
        <?php $socials = getSocialsLink($info); ?>
        <div class="social-links text-center text-md-right pt-3 pt-md-0 col-12 col-md-6">
            <a href="{{ $socials['linkedin'] }}" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            <a href="{{ $socials['dribble'] }}" class="dribble"><i class="bx bxl-dribbble"></i></a>
            <a href="{{ $socials['instagram'] }}" class="instagram"><i class="bx bxl-instagram"></i></a>
        </div>
    </div>
</footer>

<a href="#" class="back-to-top d-flex align-items-center justify-content-center">
    <i class="bi bi-arrow-up-short"></i>
</a>
