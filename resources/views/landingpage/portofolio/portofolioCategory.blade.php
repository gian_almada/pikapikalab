@extends('landingpage.portofolio')

@section('title', 'Portofolio - ' . $category->name)

@section('content')
<!-- ======= Hero Section ======= -->
<section id="heroPortofolio" class="d-flex align-items-center">
    <div class="container position-relative" style="height: 100%">
        <div class="row justify-content-center" style="margin-top: 15vh">
            <div class="col-xl-9 col-lg-9 text-center">
                <h2 id="title" data-aos="fade-up" data-aos-delay="200">{{ $category->name }}</h2>
                <p class="mb-0" id="desc" data-aos="fade-up" data-aos-delay="300">
                    {{ $category->deskripsi }}
                </p>
            </div>
        </div>
    </div>
</section><!-- End Hero -->

<!-- ======= Works Section ======= -->
<section class="section site-portfolio">
    <div class="container">
        <div id="portfolio-grid" class="row no-gutter" data-aos="fade-up" data-aos-delay="400">
            @foreach($porto as $key => $portofolio)
                @if($portofolio->display == 1)
                    <div class="item {{ getKategoriAlias($portofolio->kategori) }} col-sm-6 col-md-4 col-lg-4 mb-4">
                        <div class="item-wrap fancybox">
                            <div class="work-info">
                                <h3>{{ getClientName($portofolio->client_id) }}</h3>
                                <span>{{ getKategoriName($portofolio->kategori) }}</span>
                                <div>
                                    <a class="btn btn-outline-light" data-fancybox="gallery" href="{{ asset($portofolio->path . '/' . $portofolio->file_name) }}">View</a>
                                    <button onclick="location.href='{{ url('/portofolio/' . $alias . '/' . $portofolio->file_name) }}'" class="btn btn-outline-light">Details</button>
                                </div>
                            </div>
                            <img class="img-porto" src="{{ asset($portofolio->path . '/' . $portofolio->file_name) }}">
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</section><!-- End  Works Section -->

<section class="section services section-bg">
    <div class="container">
        <div class="row justify-content-left">
            <div class="col-xl-9 col-lg-9 text-left pb-3">
                <h3 data-aos="fade-up" data-aos-delay="100">
                    See Other Categories
                </h3>
            </div>
        </div>
        <div class="row">
            <?php
            $iterate = 1;
            ?>
            @foreach($otherCategory as $key => $kategori)
                @if($kategori->display == 1)
                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch<?php if ($key != 0 && $key != 1 && $key != 2) : ?> mt-4<?php elseif($key == 1): ?> mt-4 mt-md-0<?php elseif($key == 2): ?> mt-4 mt-lg-0<?php endif; ?>" data-aos="zoom-in" data-aos-delay="{{ $iterate * 100 }}">
                            <div class="icon-box w-100" data-color="{{$kategori->color ? $kategori->color : '007bff'}}">
                                <div class="icon">
                                    <i class="bx {{ $kategori->thumbnail ? $kategori->thumbnail : 'bx-loader-circle' }} bx-tada" style="color: #{{$kategori->color}}"></i>
                                </div>
                                <h4><a href="{{ url('/portofolio/' . $kategori->alias) }}">{{ $kategori->name }}</a></h4>
                                <p>{{ mb_strimwidth($kategori->deskripsi, 0, 100, " ...") }}</p>
                            </div>
                        </div>
                    <?php
                    $iterate = ($iterate == 3) ? 1 : $iterate + 1;
                    ?>
                @endif
            @endforeach

        </div>
        <script>
            $('.icon-box').hover(
                function(){
                    let color = $(this).data('color');
                    $(this).css('backgroundColor','#'+color);
                    $(this).find('.bx').css('color','#fff');
                },
                function(){
                    let color = $(this).data('color');
                    $(this).css('backgroundColor','#FFF');
                    $(this).find('.bx').css('color','#'+color);
                });
        </script>
    </div>
</section>
@endsection
