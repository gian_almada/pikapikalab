<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="{{ url('/') }}" class="logo mr-auto">
            <img src="{{ asset(getLogo()) }}" alt="" class="img-fluid">
        </a>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a class="lexend" href="{{ url('/') }}">Home</a></li>
                <li><a class="lexend" href="" data-toggle="modal" data-target="#modalContactForm">Contact</a></li>
            </ul>
        </nav><!-- .nav-menu -->
    </div>
</header><!-- End Header -->
