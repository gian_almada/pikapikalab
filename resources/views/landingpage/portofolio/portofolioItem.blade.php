@extends('landingpage.portofolio')

@section('title', 'Portofolio - ' . getKategoriName($porto->kategori))

@section('content')
    <a class="back-button" data-aos="fade-up" href="javascript:history.back()">
        <i class="bi bi-arrow-left-short"></i>
    </a>

    <section class="d-flex align-items-center">
        <div class="container position-relative" style="height: 100%">
            <div class="row align-items-stretch" style="margin-top: 15vh">
                <div class="col-md-12 mb-3" data-aos="fade-up" data-aos-delay="200">
                    <div class="d-flex justify-content-center align-items-center flex-column">
                        <a class="d-flex justify-content-center align-items-center" href="{{ asset($porto->path . '/' . $porto->file_name) }}" data-fancybox="gallery"
                           style="width: 100%">
                            <?php
                            $array = explode(".", $porto->file_name);
                            if (end($array) == "mp4"): ?>
                            <iframe style="width: 100%; height: 60vh; max-height: 60vh;"
                                    src="{{ asset($porto->path . '/' . $porto->file_name) }}" frameborder="0"
                                    allowfullscreen sandbox></iframe>
                            <?php else: ?>
                            <img src="{{ asset($porto->path . '/' . $porto->file_name) }}" alt="Image"
                                 class="img-fluid">
                            <?php endif; ?>
                        </a>
                        <small class="py-3 text-center"
                               style="font-style: italic"><strong>Category: </strong>{{ getKategoriName($porto->kategori) }}
                            |
                            <strong>Uploaded: </strong>{{ \Carbon\Carbon::parse($porto->created_at)->format('d M Y') }}
                            <br> <strong>&copy; Pikapika Lab</strong></small>
                    </div>
                </div>
                <hr class="my-1" data-aos="fade-up" data-aos-delay="100"
                    style="border-color: rgba(232,232,232, .6); width: 60%">
                <div class="col-md-12 ml-auto mt-3" data-aos="fade-up" data-aos-delay="200">
                    <div class="sticky-content text-center">
                        <?= html_entity_decode($porto->deskripsi, ENT_QUOTES) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section services section-bg">
        <div class="container">
            <div class="row justify-content-left">
                <div class="col-xl-9 col-lg-9 text-left pb-3">
                    <h3 data-aos="fade-up" data-aos-delay="100">
                        See Other
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div id="portfolio-grid" class="row no-gutter" data-aos="fade-up" data-aos-delay="400">
                        @foreach($otherItem as $key => $portofolio)
                            @if($portofolio->display == 1)
                                <div class="item {{ getKategoriAlias($portofolio->kategori) }} col-sm-6 col-md-4 col-lg-4 mb-4">
                                    <a href="{{ url('/portofolio/' . $alias . '/' . $portofolio->file_name) }}" class="item-wrap fancybox">
                                        <div class="work-info">
                                            <h3>{{ getClientName($portofolio->client_id) }}</h3>
                                            <span>{{ getKategoriName($portofolio->kategori) }}</span>
                                        </div>
                                        <img class="img-porto" src="{{ asset($portofolio->path . '/' . $portofolio->file_name) }}">
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <script>
                $('.icon-box').hover(
                    function(){
                        let color = $(this).data('color');
                        $(this).css('backgroundColor','#'+color);
                        $(this).find('.bx').css('color','#fff');
                    },
                    function(){
                        let color = $(this).data('color');
                        $(this).css('backgroundColor','#FFF');
                        $(this).find('.bx').css('color','#'+color);
                    });
            </script>
        </div>
    </section>
@endsection
