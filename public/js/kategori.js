let Kategori = {
    addkategoriModal: $('#modalAdd'),
    editkategoriModal: $('#modalEdit'),
    btnShowEditModal: $('.editKategori'),
    CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),

    init: function () {
        Kategori.initModalShow();
        Kategori.initCheckbox();
        Kategori.initModalClose();
    },

    initModalShow: function () {
        let btnAddKategori = $('#submitAddForm'),
            formAddKategori = $('#formAddKategori'),
            btnEditKategori = $('#submitEditForm'),
            formEditKategori = $('#formEditKategori');

        const doSubmit = function (){
            let url = '/admin/category/add',
                dataForm = new FormData(formAddKategori[0]);

            btnAddKategori.prop('disabled', true).text('Saving ..');

            Kategori.submitForm(url, dataForm, 'POST', Kategori.addkategoriModal, btnAddKategori);
        }

        const doSubmitEdit = function (){
            let url = '/admin/category/edit/' + $('#editId').val(),
                dataForm = new FormData(formEditKategori[0]);

            btnEditKategori.prop('disabled', true).text('Saving ..');

            Kategori.submitForm(url, dataForm, 'POST', Kategori.editkategoriModal, btnEditKategori);
        }

        btnAddKategori.on('click', function (e){

            e.preventDefault();

            if (!formAddKategori.parsley().validate()) {
                return;
            }

            doSubmit();
        });

        btnEditKategori.on('click', function (e){

            e.preventDefault();

            if (!formEditKategori.parsley().validate()) {
                return;
            }

            doSubmitEdit();
        });

        Kategori.btnShowEditModal.on('click', function (){
            let kategori = $(this).data('kategori'),
                color = kategori.color ? kategori.color : 'FFB344';

            $('#editId').val(kategori.id);
            $('#editName').val(kategori.name).parent().addClass('is-filled');
            $('#editAlias').val(kategori.alias).parent().addClass('is-filled');
            $('#editThumbnail').val(kategori.thumbnail).parent().addClass('is-filled');
            $('#editDesc').val(kategori.deskripsi).parent().addClass('is-filled');

            $('#editColorField')
                .val(color)
                .parent().parent().addClass('is-filled');

            $('#editColorPicker')
                .css('background-color', '#' + color);

            let colorPickerEdit = new Picker({
                parent: document.querySelector('#editColorPicker'),
                alpha: false, // default: true
                color: '#' + color,
                defaultColor: '#' + color,
                editorFormat: 'hex',
                popup: 'right',
                onChange: function(color) {
                    document.querySelector('#editColorPicker').style.backgroundColor = color.rgbaString;
                    document.querySelector('#editColorField').value = (color.hex).substring(1, 7);
                },
            })
        });
    },

    initModalClose: function () {
        Kategori.addkategoriModal.on('hidden.bs.modal', function(e) {
            $('#formAddKategori').parsley().reset();
            $('#formAddKategori').trigger('reset');
        });

        Kategori.editkategoriModal.on('hidden.bs.modal', function(e) {
            $('#formEditKategori').parsley().reset();
            $('#formEditKategori').trigger('reset');
        });
    },

    initCheckbox: function () {
        $('input[type="checkbox"]').change(function (){
            let id = $(this).data("id");

                $.ajax({
                    headers: {'X-CSRF-TOKEN': Kategori.CSRF_TOKEN},
                    url : '/admin/category/display',
                    type : 'POST',
                    data : {
                        id: id,
                        display: this.checked ? 1 : 0
                    },
                    dataType:'json',
                });
        });
    },

    submitForm: function (url, data, type, modal, btn) {

        return $.ajax({
            headers: {'X-CSRF-TOKEN': Kategori.CSRF_TOKEN},
            url : url,
            type : type,
            data : data,
            contentType: false,
            processData: false,
            success: function (data){
                modal.modal('hide');
                btn.prop('disabled', false).text('Submit');

                swal.fire({
                    title: 'Success',
                    text: data.msg,
                    icon: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Close'
                }).then((result) => {
                    if(result){
                        // Do Stuff here for success
                        location.reload();
                    }
                });
            },
            error: function (xhr, status, error){
                modal.modal('hide');

                swal.fire({
                    title: 'Failed!',
                    text: xhr.responseText,
                    icon: 'error',
                    confirmButtonText: 'Close'
                });
            }
        });
    }
}

Kategori.init();

let ColorPicker = {
    init: function () {
        ColorPicker.initVanillaPicker();
    },

    initVanillaPicker: function () {

        let parentElAdd = document.querySelector('#addColorPicker'),
            fieldElAdd  = document.querySelector('#addColorField')

       if (parentElAdd) {
           let colorPickerAdd = new Picker({
               parent: parentElAdd,
               alpha: false, // default: true
               color: '#FFB344',
               defaultColor: '#FFB344',
               editorFormat: 'hex',
               popup: 'right',
               onChange: function (color) {
                   parentElAdd.style.backgroundColor = color.rgbaString;
                   fieldElAdd.value = (color.hex).substring(1, 7);
               },
           });
       }
    }
}

window.addEventListener('load', ColorPicker.init);
