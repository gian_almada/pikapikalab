demo = {
  initDocumentationCharts: function() {
    if ($('#dailySalesChart').length != 0 && $('#websiteViewsChart').length != 0) {
      /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

      dataDailySalesChart = {
        labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
        series: [
          [12, 17, 7, 17, 23, 18, 38]
        ]
      };

      optionsDailySalesChart = {
        lineSmooth: Chartist.Interpolation.cardinal({
          tension: 0
        }),
        low: 0,
        high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
      }

      var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

      var animationHeaderChart = new Chartist.Line('#websiteViewsChart', dataDailySalesChart, optionsDailySalesChart);
    }
  },
}

const Password = {
    CSRF_TOKEN: $('meta[name="csrf-token"]').attr('content'),
    form: $('#formChangePassword'),
    btnSubmit: $('#submitChangePassword'),
    userID: $('#id_user'),

    init: function () {
        Password.initParsley();
        Password.initBtnSubmit();
        Password.initModal();
    },

    initModal: function () {
        $('#modalChangePassword').on('hidden.bs.modal', function () {
            $('#formChangePassword').trigger("reset");
            Password.form.parsley().reset();
            $(".bmd-label-floating").css('color', '#495057');
            $(".bmd-form-group").removeClass('is-filled');
        });
    },

    initParsley: function () {
        window.Parsley.addValidator('oldpassword', {
            validateString: function(value) {
                let status = '',
                    values = [];

                values['userID'] = Password.userID.val();
                values['oldPassword'] = value;

                let data = Password.createFormData(values);

                $.ajax({
                    headers: {'X-CSRF-TOKEN': Password.CSRF_TOKEN},
                    url : '/checkOldPassword',
                    method : 'POST',
                    data : data,
                    processData: false,
                    contentType: false,
                    async: false,
                    cache: false,
                    success : function(data) {
                        status = data.status;
                    },
                });

                return status;
            },
            messages: {
                en: 'User not found or old password not correct !',
            }
        });

        $('#oldPassword').parsley().on('field:success', function() {
            $("label[for='oldPassword']").css('color', '#28a745');
        });
        $('#oldPassword').parsley().on('field:error', function() {
            $("label[for='oldPassword']").css('color', '#dc3545');
        });

        $('#newPassword').parsley().on('field:success', function() {
            $("label[for='newPassword']").css('color', '#28a745');
        });
        $('#newPassword').parsley().on('field:error', function() {
            $("label[for='newPassword']").css('color', '#dc3545');
        });

        $('#confirmNewPassword').parsley().on('field:success', function() {
            $("label[for='confirmNewPassword']").css('color', '#28a745');
            $('#submitChangePassword').prop('disabled', false);
        });
        $('#confirmNewPassword').parsley().on('field:error', function() {
            $("label[for='confirmNewPassword']").css('color', '#dc3545');
            $('#submitChangePassword').prop('disabled', true);
        });

    },

    initBtnSubmit: function () {
        Password.btnSubmit.on('click', function (e) {
            e.preventDefault();

            if (!Password.form.parsley().validate()) {
                return;
            }

            Password.doSubmit();
        });
    },

    doSubmit: function () {
        let values = [],
            newPassword = $('#newPassword').val(),
            userID = Password.userID.val(),
            changePasswordModal = $('#modalChangePassword'),
            submitBtn = $('#submitChangePassword');

            values['userID'] = userID;
            values['newPassword'] = newPassword;

        let data = Password.createFormData(values);

        submitBtn.prop('disabled', true);
        submitBtn.html('Submitting....');

        Password.callApi('/changePassword', data, function(respoonse, status){
            if (status != 200) {
                return;
            }

            submitBtn.html('Submit');
            changePasswordModal.modal('hide');

            swal.fire({
                title: 'Success',
                text: respoonse.msg,
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Close'
            });
        });
    },

    /**
     * @param {string} url
     * @param {FormData} data
     * @param {function} callback
     */
    callApi: function (url, data, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.setRequestHeader('X-CSRF-TOKEN', Password.CSRF_TOKEN);
        xhr.onload = function () {
            callback(JSON.parse(this.responseText), this.status);
        };
        xhr.send(data);
    },

    /**
     * @param values
     * @returns {FormData}
     */
    createFormData: function (values) {
        let formData = new FormData();

        for (var key in values) {
            formData.append(key, values[key]);
        }

        return formData;
    },
}

window.addEventListener('load', Password.init);
